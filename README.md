# GlinttSupport

### Dependencias usadas ###

[sails-postgresql](https://www.npmjs.com/package/sails-postgresql)

[fb](https://www.npmjs.com/package/fb)

[twit](https://www.npmjs.com/package/twit)

[passport](https://www.npmjs.com/package/passport)

[passport-local](https://www.npmjs.com/package/passport-local)

[passport-facebook](https://www.npmjs.com/package/passport-facebook)

[passport-twitter](https://www.npmjs.com/package/passport-twitter)

### Chamadas disponíveis ###
**Support e Supervisor**

** '/login'**	- POST username e password - Faz login de Supervisor e Support.

** '/logout'**	- Termina a sessão atual.

** '/info'**	- Devolve informação do utilizador atual.

** '/support/password'** - POST old e new - Altera a password para "new" se "old" for a antiga password.

** '/support/post/accept/:id'** - Marca o Post dado por ":id" para o utilizador Support autenticado.

** '/support/post/solve/:id' **	- Marca um Post da área do utilizador autenticado dado por ":id" como resolvido.

** '/support/post/solve/quick ** - Marca um Post da barra lateral dado por ":id" como resolvido (fica associado ao utilizador logado).

** '/support/post/answer' ** - POST id e message - Responde com "message" o id do Post com o nome da aplicação.

** '/support/post/refresh/:id' ** - Devolve o post atualizado enviado com o id. Esta atualização inclui novos posts nas redes sociais.

** 'post?where={"support":idSupport,"state":"in progress"}' ** - Devovle os posts da área pessoal do support idSupport.

** 'post?sort=date ASC&where={"state":"queue"}' ** - Devolve os posts da queue.

** 'post/:id' ** - Devolve a informação do post com id ":id". Esta chamada devolve os replies das postlines também.


**Cliente**

 **'/client/login/facebook'** - Faz login de cliente pelo Facebook (abre pagina facebook, não tem parametros)

** '/client/login/twitter'**      - Faz login de cliente pelo Twitter (abre pagina twitter, não tem parametros)

** '/client/login/google'**      - Faz login de cliente pelo Twitter (abre pagina twitter, não tem parametros)