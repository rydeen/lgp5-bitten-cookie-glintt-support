/**
 * Default model configuration
 * (sails.config.models)
 *
 * Unless you override them, the following properties will be included
 * in each of your models.
 *
 * For more info on Sails models, see:
 * http://sailsjs.org/#/documentation/concepts/ORM
 */

module.exports.models = {

  /***************************************************************************
   *                                                                          *
   * Your app's default connection. i.e. the name of one of your app's        *
   * connections (see `config/connections.js`)                                *
   *                                                                          *
   ***************************************************************************/
  connection: 'heroku',

  /***************************************************************************
   *                                                                          *
   * How and whether Sails will attempt to automatically rebuild the          *
   * tables/collections/etc. in your schema.                                  *
   *                                                                          *
   * See http://sailsjs.org/#/documentation/concepts/ORM/model-settings.html  *
   *                                                                          *
   ***************************************************************************/
  migrate: 'alter',

  /**
   * This method adds records to the database
   *
   * To use add a variable 'seedData' in your model and call the
   * method in the bootstrap.js file
   */
  seed: function (callback) {
    var self = this;
    var modelName = self.adapter.identity;

    if (!self.seedData) {
      //sails.log.debug('No data avaliable to seed ' + modelName);
    } else {
      //sails.log.debug('Seeding ' + modelName + '...');

      self.seedData.forEach(function (seedObj, index) {
        self.findOrCreate(seedObj, seedObj).exec(function (err, results) {
          if (err) {
            //sails.log.debug(err);
          } else {
            //sails.log.debug(modelName + ' ' + index + 'th seed planted');
          }
        });
      });
    }

    callback();
  },

  /**
   * Deletes a database table's content
   */
  clear: function (callback) {
    var self = this;
    var modelName = this.adapter.identity;

    //sails.log.debug('Destroying ' + modelName + '...');

    self.destroy({}).exec(function (err, results) {
      if (err) {
        //sails.log.debug(err);
      } else {
        //sails.log.debug(modelName + ' destroyed');
      }
    });

    callback();
  }
};
