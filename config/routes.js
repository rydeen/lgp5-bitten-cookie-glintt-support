/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#/documentation/concepts/Routes/RouteTargetSyntax.html
 */
var passport = require('passport');
module.exports.routes = {

  /**
   * Angular entry-points
   */

  '/': {
    view: 'client'
  },

  '/client': {
    view: 'client'
  },

  '/support': {
    view: 'support'
  },

  '/supervisor': {
    view: 'supervisor'
  },

  '/loginpage': {
    view: 'login'
  },

  /**
   * API
   */

  /***Support and Supervisor***/
  '/login': 'Auth.login',
  '/logout': 'Auth.logout',
  '/info' : 'Auth.info',

  '/support/refresh' : 'SupportUserController.refresh', // ainda so força a chamada as bd's
  '/support/post/accept/:id' : 'PostController.accept',
  '/support/post/solve/:id' : 'PostController.solve',
  '/support/post/solve/quick/:id': 'PostController.quicksolve',
  '/support/post/answer'  : 'PostController.answer', // POST com "id" e "message"
  '/support/post/:id' : 'PostController.list',
  '/support/post/refresh/:id' : 'PostController.refreshPost',
  '/supportuser/listall': 'SupportUserController.listAll',
  //'/support/chat/accept/:id' : 'PostController.accept',
  //'/support/chat/solve/:id' : 'PostController.accept',

  '/support/password' : 'SupportUserController.changePassword', // POST com "old", "new"

  /*****************/

  /*** Cliente ***/
  '/client/login/facebook' : passport.authenticate('facebook', { scope: [ 'email', 'user_birthday' ] }),
  '/client/login/facebook/callback' : passport.authenticate('facebook', { successRedirect: '/', failureRedirect: '/client/login/facebook' }),

  '/client/login/twitter' : passport.authenticate('twitter'),
  '/client/login/twitter/callback' : passport.authenticate('twitter', { successRedirect: '/', failureRedirect: '/client/login/twitter' }),

  '/client/login/google' : passport.authenticate('google', { scope: ['https://www.googleapis.com/auth/plus.login', 'https://www.googleapis.com/auth/userinfo.email'] }),
  '/client/login/google/callback' : passport.authenticate('google', { successRedirect: '/', failureRedirect: '/client/login/google' }),

  '/client/info' : 'CustomerUserController.info',
  '/client/logout' : 'CustomerUserController.logout',
  /*****************/

  /*** testes ****/
  '/session' : 'SocialController.session',

};
