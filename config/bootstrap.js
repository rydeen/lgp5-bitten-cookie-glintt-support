/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://sailsjs.org/#/documentation/reference/sails.config/sails.config.bootstrap.html
 */

module.exports.bootstrap = function(cb) {

  // It's very important to trigger this callback method when you are finished
  // with the bootstrap!  (otherwise your server will never lift, since it's waiting on the bootstrap)

    async.series([
        SupportUser.clear,
        SupervisorUser.clear,
        PostLine.clear,
        Post.clear,
        PostReply.clear,
        Hashtag.clear,
        FavoriteFile.clear,
        CustomerUser.clear,
        ChatLine.clear,
        Chat.clear,
        EnteredQueueDatetime.clear,
        ExitedQueueDatetime.clear,
        SolvedDatetime.clear,
        SupportUser.seed,
        SupervisorUser.seed,
        Hashtag.seed,
        //PostLine.seed,
        //Post.seed,
        //FavoriteFile.seed,
        //CustomerUser.seed,
        //ChatLine.seed,
        //Chat.seed
    ]);

    
    // TODO:(?) CRON parece ser uma melhor solução em vez do setInterval. Está em fase de pesquisa 
    // Chama ao iniciar e depois de 5 em 5 min

    // Chama a primeira vez alguns segundos apos iniciar a aplicação
    setTimeout( function(){ 
        sails.services.socialnetworks.fetchPosts();;
    }, 3000);

    //setTimeout( function() {
    //    sails.services.facebook.fetchPagePosts();
    //}, 1000);

    
    var minutes = 5;
    var interval = minutes * 60 * 1000;
    var teste = 5 * 1000;
    setInterval(function() {
        sails.services.socialnetworks.fetchPosts();
    }, interval);
    
    
    cb();
};
