/**
* passport.js
*
* Configuring passport to be used as
* a middleware to passport-local
*
**/

var passport = require('passport'),
LocalStrategy = require('passport-local').Strategy,
FacebookStrategy = require('passport-facebook').Strategy,
TwitterStrategy = require('passport-twitter').Strategy;
GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

module.exports = {
  http: {
    customMiddleware: function(app){
      //console.log('Express midleware for passport');
      app.use(passport.initialize());
      app.use(passport.session());
    }
  }
};
