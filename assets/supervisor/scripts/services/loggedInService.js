angular.module('GlinttSupportSupervisor').factory("loggedInService", function($q, $http, $window){
  return {
    checkLoggedIn: function(){
      var deferred = $q.defer();

      $http.get('/info').success(function(response) {
        if(response.success && response.data && response.data.permissions == 'supervisor')
          deferred.resolve();
        else {
          deferred.reject();
          $window.location.href = '/loginpage';
        }
      });

      return deferred.promise;
    }
  };
});
