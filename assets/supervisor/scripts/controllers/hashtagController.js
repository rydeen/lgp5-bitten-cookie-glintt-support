'use strict';

angular.module('GlinttSupportSupervisor.controllers').
  controller('HashtagCtrl', ['$scope', '$http', function ($scope, $http) {
    $scope.hashtags = [];
    $scope.newHashtag = "";

    var getHashtags = function () {
      $http.get('hashtag')
        .success(function (msg) {
          $scope.hashtags = msg;
        })
        .error(function (msg) {
          console.error(msg);
        });
    };

    $scope.deleteHashtag = function (index) {
      $http.get("/hashtag/destroy/" + $scope.hashtags[index].id)
        .success(function () {
          $scope.hashtags.splice(index, 1);
        })
        .error(console.error);
    };

    $scope.addHashtag = function () {
      if ($scope.newHashtag !== "") {
        $http.get("/hashtag/create?hashtag=" + $scope.newHashtag)
          .success(function (msg) {
            $scope.hashtags.push(msg);
          })
          .error(console.error);

        $scope.newHashtag = "";
      }
    };

    getHashtags();
  }]);
