'use strict';

angular.module('GlinttSupportSupervisor.controllers').
  controller('RegisterCtrl', ['$scope', '$http', function ($scope, $http) {
    $scope.successMessage = "";
    $scope.errorMessage = "";

    $scope.registerSupport = function() {
      $scope.successMessage = "";
      $scope.errorMessage = "";

      $http.get('/supportuser/create?username=' + $scope.newUsername + '&password=' + $scope.newPassword)
        .success(function() {
          $scope.successMessage = "Utilizador criado com sucesso";
          $scope.errorMessage = "";
        })
        .error(function(msg) {
          console.error(msg);
          $scope.successMessage = "";
          $scope.errorMessage = "Houve um problema a criar o utilizador:";

          if(msg.invalidAttributes.id)
            $scope.errorMessage += "\n" + msg.invalidAttributes.id[0].message;

          if(msg.invalidAttributes.username)
            $scope.errorMessage += "\n" + msg.invalidAttributes.username[0].message;

          if(msg.invalidAttributes.password)
            $scope.errorMessage += "\n" + msg.invalidAttributes.password[0].message;
        });
    };
  }]);
