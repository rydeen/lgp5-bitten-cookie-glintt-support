'use strict';

angular.module('GlinttSupportSupervisor.controllers').
  controller('StatisticsCtrl', ['$scope', '$http', '$route',
    function ($scope, $http, $route) {
      $scope.lastUpdate = new Date();
      $scope.type = $route.current.params.type;
      var i, j, hoursInDay = 24, daysInMonth = 31, monthsInYear = 12;


      $scope.labelsBarDaily = ['24h'];
      for (i = 1; i < hoursInDay; i++)
        $scope.labelsBarDaily.push(i + 'h');

      $scope.labelsBarMonthly = [];
      for (i = 1; i <= daysInMonth; i++)
        $scope.labelsBarMonthly.push(i);

      $scope.labelsBarYearly = ['Janeiro', 'Fevereiro', 'Março',
        'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro',
        'Outubro', 'Novembro', 'Dezembro'];


      $scope.coloursThreeBar = [
        {
          fillColor: "#cbcbcb",
          strokeColor: "rgba(220,220,220,0.8)",
          highlightFill: "#ECECEC",
          highlightStroke: "rgba(220,220,220,1)"
        },
        {
          fillColor: "#f39622",
          strokeColor: "rgba(220,220,220,0.8)",
          highlightFill: "#F3BC24",
          highlightStroke: "rgba(220,220,220,1)"
        },
        {
          fillColor: "#56B257",
          strokeColor: "rgba(220,220,220,0.8)",
          highlightFill: "#63D864",
          highlightStroke: "rgba(220,220,220,1)"
        }
      ];
      $scope.coloursOneBar = [
        {
          fillColor: "#56B257",
          strokeColor: "rgba(220,220,220,0.8)",
          highlightFill: "#63D864",
          highlightStroke: "rgba(220,220,220,1)"
        }
      ];


      $scope.labelsPie = ["Em espera", "Em resolução", "Resolvidas"];

      $scope.coloursPie = [
        {
          strokeColor: "#cbcbcb",
          highlight: "#ECECEC"
        },
        {
          strokeColor: "#f39622",
          highlight: "#F3BC24"
        },
        {
          strokeColor: "#56B257",
          highlight: "#63D864"
        }
      ];

      $scope.dataPie = [0, 0, 0];

      $scope.dataBarDaily = [[], [], []];
      for (j = 0; j <= 2; j++)
        for (i = 1; i <= hoursInDay; i++)
          $scope.dataBarDaily[j].push(0);

      $scope.dataBarMonthly = [[]];
      for (i = 1; i <= daysInMonth; i++)
        $scope.dataBarMonthly[0].push(0);

      $scope.dataBarYearly = [[]];
      for (i = 1; i <= monthsInYear; i++)
        $scope.dataBarYearly[0].push(0);

      const queuePos = 0, inprogressPos = 1, solvedPos = 2;
      $scope.queuePos = queuePos;
      $scope.inprogressPos = inprogressPos;
      $scope.solvedPos = solvedPos;

      var getMessages = function () {
        $http.get('/' + $scope.type.slice(0, $scope.type.length - 1) + '/getThisYear')
          .success(function (data) {
            if (!data) {
              console.error(data);
              return;
            }

            // Format: {State, Time}
            var processedValues = [], processedValuesSolved;

            // Argument is named post, but can be a chat,
            // depending on $scope.type
            data.forEach(function (post) {
              for (var i = 0; i < post.enteredQueue.length; i++)
                if (post.solvedDatetime.length > i)
                  processedValues.push({state: 'solved', time: post.solvedDatetime[i].date});
                else if (post.exitedQueue.length > i)
                  processedValues.push({state: 'in progress', time: post.exitedQueue[i].date});
                else
                  processedValues.push({state: 'queue', time: post.enteredQueue[i].date});
            });

            processedValues.filter(function (post) {
              return new Date(post.time).getDay() == $scope.lastUpdate.getDay();
            }).forEach(function (value) {
              var position = getStatePosition(value.state);

              if (position !== -1) {
                $scope.dataPie[position] += 1;
                $scope.dataBarDaily[position][new Date(value.time).getHours()] += 1;
              }
            });

            processedValuesSolved = processedValues.filter(function (post) {
              return post.state == 'solved';
            });

            processedValuesSolved.filter(function (post) {
              return new Date(post.time).getMonth() == $scope.lastUpdate.getMonth();
            }).forEach(function (value) {
              $scope.dataBarMonthly[0][new Date(value.time).getDay()] += 1;
            });

            processedValuesSolved.forEach(function (post) {
              $scope.dataBarYearly[0][new Date(post.time).getMonth()] += 1;
            });
          })
          .error(console.error);
      };
      getMessages();

      var getStatePosition = function (state) {
        var position;

        if (state == 'queue')
          position = queuePos;
        else if (state == 'solved')
          position = solvedPos;
        else if (state == 'in progress')
          position = inprogressPos;
        else {
          console.error("Unknowen state " + value.state);
          return -1;
        }

        return position;
      };
    }]);
