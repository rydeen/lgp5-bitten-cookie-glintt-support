'use strict';

angular.module('GlinttSupportSupervisor.controllers').
    controller('InfoCtrl', ['$scope', '$http', function ($scope, $http) {
        $scope.users = [];

        var getUsers = function () {
            // TODO mudar blueprint para método do controlador
            $http.get('/supportuser/listall')
                .success(function (msg) {
                    if (!msg) {
                        console.error(msg);
                        return;
                    }

                    $scope.users = [];

                    msg.forEach(function (user) {
                        $scope.users.push(user);
                    });

                })
                .error(console.error);
        };

        $scope.countInProgress = function(array){
                return array.filter(function(post) { return post.state == "in progress" }).length;
        };

        $scope.countSolved = function(array, lastLogin){
            return array.filter(function(post) { return post.state == "solved" && post.solvedTime > lastLogin  }).length;
        };

        var subscribeToSocket = function () {
            if (!io.socket.alreadyListeningToOrders) {
                io.socket.alreadyListeningToOrders = true;

                io.socket.on('post', function onServerSentEvent(msg) {
                    switch (msg.verb) {
                        case 'addedTo':
                            getUsers();
                            break;
                        case 'updated':
                            getUsers();
                            break;
                        case 'created':
                            getUsers();
                            break;
                }
                });

                io.socket.on('chat', function onServerSentEvent(msg) {
                    switch (msg.verb) {
                        case 'addedTo':
                            getUsers();
                            break;
                        case 'updated':
                            getUsers();
                            break;
                        case 'created':
                            getUsers();
                            break;
                    }
                });

                io.socket.on('supportuser', function onServerSentEvent(msg) {
                    console.log("ola");
                    switch (msg.verb) {
                        case 'addedTo':
                            getUsers();
                            break;
                        case 'updated':
                            getUsers();
                            break;
                        case 'created':
                            getUsers();
                            break;
                    }
                });

                io.socket.get('/post/postUpdateSubscribe');
                io.socket.get('/chat/chatUpdateSubscribe');
                io.socket.get('/supportuser/subscribe');

            }
        };

        getUsers();
        subscribeToSocket();

    }]);
