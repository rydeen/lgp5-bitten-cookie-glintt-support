'use strict';

angular.module('GlinttSupportSupervisor', [
  'ngRoute',
  'chart.js',
  'GlinttSupportSupervisor.controllers'
])
  .config(['$routeProvider', function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'supervisor/views/index.html',
        resolve: {
          loggedin: function (loggedInService) {
            return loggedInService.checkLoggedIn();
          }
        }
      })
      .when('/preferences', {
        templateUrl: 'supervisor/views/preferences.html',
        resolve: {
          loggedin: function (loggedInService) {
            return loggedInService.checkLoggedIn();
          }
        }
      })
      .when('/statistics/:type', {
        templateUrl: 'supervisor/views/statistics.html',
        controller: 'StatisticsCtrl',
        resolve: {
          loggedin: function (loggedInService) {
            return loggedInService.checkLoggedIn();
          }
        }
      })
      .when('/registersupport', {
        templateUrl: 'supervisor/views/registersupport.html',
        controller: 'RegisterCtrl',
        resolve: {
          loggedin: function (loggedInService) {
            return loggedInService.checkLoggedIn();
          }
        }
      })
      .when('/info', {
        templateUrl: 'supervisor/views/info.html',
        resolve: {
          loggedin: function (loggedInService) {
            return loggedInService.checkLoggedIn();
          }
        }
      })
      .otherwise({redirectTo: '/'});
  }])
  .controller('LogoutCtrl', ['$scope', '$http', '$location',
    function ($scope, $http, $location) {

      $scope.logout = function () {
        $http.get('/logout').success(function () {
          $location.path('/loginpage');
        })
          .error(console.error);
      };

      $scope.userInfo = $scope.userInfo || {};

      var getUserInfo = function () {
        $http.get('/info').success(function (msg) {
          if (!msg) {
            console.error(msg);
            return;
          }

          if (msg.data)
            $scope.userInfo = msg.data;
        });
      };

      if (!$scope.userInfo.data)
        getUserInfo();
    }]);

angular.module('GlinttSupportSupervisor.controllers', []);
