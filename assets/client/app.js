'use strict';

// Declare app level module which depends on filters, and services
angular.module('GlinttSupportClient', [
  'ngRoute',
  'ngSanitize',
  'GlinttSupportClient.controllers'
]).
config(['$routeProvider', function($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'client/views/index.html',
        resolve: {
          loggedout: function(loggedInService) {
            return loggedInService.checkLoggedOut();
          }
        }
      })
      .when('/chat', {
        templateUrl: 'client/views/chat.html',
        controller: 'ChatCtrl',
        resolve: {
          loggedin: function (loggedInService) {
            return loggedInService.checkLoggedIn();
          }
        }
      })
      .otherwise({redirectTo: '/'});
}]).
controller('LogoutCtrl', ['$scope','$http', '$location', 
    function($scope, $http, $location)
    {
        $scope.logout = function(){
            $http.get('/client/logout')
                .success( function(){
                    $location.path('/');
                })
                .error( console.error );
        };
    }
]);

angular.module('GlinttSupportClient.controllers', []);
