angular.module('GlinttSupportClient').factory("loggedInService", function ($q, $http, $window, $location) {
  return {
    checkLoggedIn: function () {
      var deferred = $q.defer();

      $http.get('/info').success(function (response) {
        if (response.success && response.data && response.data.permissions == 'client')
          deferred.resolve();
        else {
          deferred.reject();
          $window.location.href = '/client';
        }
      });

      return deferred.promise;
    },

    checkLoggedOut: function () {
      var deferred = $q.defer();

      $http.get('/info').success(function (response) {
        if (response.success && response.data) {

          if (response.data.permissions == 'client') {
            deferred.reject();
            $location.path('/chat');
          } else {
            $http.get('/logout').success(function () {
              deferred.resolve();
            });
          }
        }
        else {
          deferred.resolve();
        }
      });

      return deferred.promise;
    }
  };
});
