'use strict';

angular.module('GlinttSupportClient.controllers').
  controller('HomeNaveCtrl', ['$scope', function ($scope) {
  
  
      $scope.goTo = function( srcElementID, destElementID ) {
            $('.nav-bar-li').removeClass('active');
            $(srcElementID).addClass('active'); 
            $('html, body').animate({scrollTop: $(destElementID).offset().top}, 1000);
      };
      
      $scope.scrollToTop = function() {
          $('html, body').animate({scrollTop: 0}, 1000);
      };
  
  
  }]);