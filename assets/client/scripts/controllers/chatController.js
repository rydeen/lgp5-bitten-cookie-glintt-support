'use strict';

angular.module('GlinttSupportClient.controllers').
  controller('ChatCtrl', ['$scope', '$http', function ($scope, $http) {
    $scope.userInfo = $scope.userInfo || {};
    $scope.messages = $scope.messages || [];
    $scope.newMessage = "";
    $scope.chatId = $scope.chatId || -1;

    $scope.sendMessage = function () {
      if ($scope.newMessage !== "") {
        $http.post('/chatline/newChatLine', {
          from: 'customer',
          chat: $scope.chatId,
          message: $scope.newMessage
        })
          .error(console.error);

        $scope.newMessage = "";
      }

    };

    $scope.uploadFileDropbox = function (){

        var options = {
            success: function(files) {
                $scope.newMessage = $scope.userInfo.username + " submeteu o ficheiro <a href=\"" + files[0].link +"\">" + files[0].name +"</a>, que estará disponível nas próximas 4 horas.";
                $scope.sendMessage();
            },

            // Optional. Called when the user closes the dialog without selecting a file
            // and does not include any parameters.
            cancel: function() {

            },
            linkType: "direct",
            multiselect: false
        };

        //closes modal window
        document.getElementById("attachModal").style.display = 'none';

        Dropbox.choose(options);

    };

    $scope.uploadFileGoogleDrive = function (){

        // The Browser API key obtained from the Google Developers Console.
        // Replace with your own Browser API key, or your own key.
        var developerKey = 'AIzaSyD5N3p4dly3EvD13GVV54XVhwPJLEQ0YWM';

        // The Client ID obtained from the Google Developers Console. Replace with your own Client ID.
        var clientId = "160376714211-ab8urr1jfdtkpduvi8a2lprh614b9ah6.apps.googleusercontent.com";

        // Replace with your own App ID. (Its the first number in your Client ID)
        var appId = "160376714211";

            // Scope to use to access user's Drive items.
            var scope = ['https://www.googleapis.com/auth/drive'];


        // Scope to use to access user's Drive items.
        var scope = ['https://www.googleapis.com/auth/drive'];

        var pickerApiLoaded = false;
        var oauthToken;

        function onAuthApiLoad() {
            window.gapi.auth.authorize(
                {
                    'client_id': clientId,
                    'scope': scope,
                    'immediate': false
                },
                handleAuthResult);
        }

        function onPickerApiLoad() {
            pickerApiLoaded = true;
            createPicker();
        }

        function handleAuthResult(authResult) {
            if (authResult && !authResult.error) {
                oauthToken = authResult.access_token;
                createPicker();
            }
        }

        // Create and render a Picker object for searching images.
        function createPicker() {
            if (pickerApiLoaded && oauthToken) {
                //closes modal window
                document.getElementById("attachModal").style.display = 'none';

                //creates the new modal window
                var view = new google.picker.View(google.picker.ViewId.DOCS);

                var picker = new google.picker.PickerBuilder()
                    .setAppId(appId)
                    .setOAuthToken(oauthToken)
                    .addView(view)
                    .addView(new google.picker.DocsView().setOwnedByMe(true).setIncludeFolders(true))
                    .addView(new google.picker.DocsUploadView())
                    .setDeveloperKey(developerKey)
                    .setCallback(pickerCallback)
                    .setTitle("Escolha o ficheiro para submeter")
                    .setOrigin(window.location.protocol + '//' + window.location.host )
                    .setLocale('pt-PT')
                    .build();
                picker.setVisible(true);
            }
        }

        // A simple callback implementation.
        function pickerCallback(data) {
            if (data.action == google.picker.Action.PICKED) {
                var fileId = data.docs[0].id;

                var request = gapi.client.request({
                    'path': 'https://www.googleapis.com/drive/v2/files/'+fileId,
                    'method': 'GET'
                });



                request.execute(function(resp) {
                    var downloadURL;

                    if( typeof resp.downloadUrl === 'undefined'){
                        downloadURL = resp.exportLinks['application/pdf'];

                    } else downloadURL = resp.downloadUrl;

                    downloadURL += '&access_token=' + oauthToken;

                    $scope.newMessage = $scope.userInfo.username + " submeteu o ficheiro <a href=\"" + downloadURL +"\">" + resp.title +"</a>, que estará disponível nas próximas 8 horas.";
                    $scope.sendMessage();
                });

            }
        }

        // Use the Google API Loader script to load the google.picker script.
        gapi.load('auth', {'callback': onAuthApiLoad});
        gapi.load('picker', {'callback': onPickerApiLoad});
    };

    var subscribeToSocket = function () {
      if (!io.socket.alreadyListeningToOrders) {
        io.socket.alreadyListeningToOrders = true;

        // TODO não usar parâmetro, servidor usar o ID da sessão
        io.socket.get('/chat/subscribeChatSocket', {type: 'customer', id: $scope.userInfo.id});

        io.socket.on('chat', function onServerSentEvent(msg) {
          switch (msg.verb) {
            case 'addedTo':
              $http.get('/chatline/' + msg.addedId)
                .success(function (msg) {
                  if (!msg) {
                    console.error(msg);
                    return;
                  }

                  $scope.messages.push(msg);
                  updateScroll() ;
                })
                .error(console.error);

              $scope.$apply();
              break;

            default:
              return;
          }
        });
      }
    };

    var getMessages = function () {
      $http.get('/chat?customer=' + $scope.userInfo.id)
        .success(function (msg) {
          if (!msg) {
            console.error(msg);
            return;
          }

          if (msg[0]) {
            $scope.chatId = msg[0].id;
            $scope.messages = msg[0].chatLines;
          }
        })
        .error(console.error);
    };

    if (!$scope.userInfo.data) {
      $http.get('/info')
        .success(function (msg) {
          if (!msg) {
            console.error(msg);
            return;
          }

          if (msg.data) {
            $scope.userInfo = msg.data;

            getMessages();
            subscribeToSocket();
            updateScroll() ;
          }
        })
        .error(console.error);
    } else {
      getMessages();
      subscribeToSocket();
    }
      
      
      /* SCROLL UPDATE */
      
    var updateScroll = function() {
        $("html, body").animate({ scrollTop: $(document).height() }, 1000);
    }
      
      
  }]);
