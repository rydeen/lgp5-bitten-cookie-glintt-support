'use strict';

angular.module('GlinttSupportLogin.controllers').
  controller('LoginCtrl', ['$scope', '$window', '$http', function ($scope, $window, $http) {
    $scope.user = {username: "", password: ""};
    $scope.error_message = "";

    $scope.redirectLogin = function (response) {
      if (response.success && response.data) {
        if (response.data.permissions == 'supervisor')
          $window.location.href = '/supervisor';
        else if (response.data.permissions == 'support')
          $window.location.href = '/support';

        return true;
      }

      return false;
    };

    $scope.checkLoggedIn = function () {
      $http.get('/info').success($scope.redirectLogin);
    };

    $scope.login = function () {
      $http.post('/login', $scope.user).success(function (response) {
        if ($scope.redirectLogin(response))
          return;

        $scope.error_message = response.description;
      });
    };
  }]);
