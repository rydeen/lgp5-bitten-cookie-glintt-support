'use strict';

// Declare app level module which depends on filters, and services
angular.module('GlinttSupportLogin', [
  'ngRoute',
  'GlinttSupportLogin.controllers'
]).
  config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/', {templateUrl: 'login/views/index.html', controller: 'LoginCtrl'});
    $routeProvider.otherwise({redirectTo: '/'});
  }]);

angular.module('GlinttSupportLogin.controllers', []);
