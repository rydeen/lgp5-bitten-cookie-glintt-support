'use strict';

angular.module('GlinttSupportSupport.controllers').
controller('PostsCtrl', ['$scope', '$http','$timeout', function ($scope, $http,$timeout) {

  $scope.userInfo = $scope.userInfo || {};
  $scope.addChat = $scope.addChat || -1;
  $scope.error_message = $scope.error_message || {};
  $scope.posts = $scope.posts || {};
  $scope.expand = false;

  $scope.replies_post = $scope.replies || {};
  $scope.replies_postLine = $scope.replies || {};
  $scope.aux = 0;
  $scope.imageSourceExpand="expand.png";
  /* <MESSAGES> */

  $scope.resolvePost = function (index) {
    $http.post('/support/post/solve/'+index, {
     id: index,
     user: $scope.userInfo.id
   }).error(console.error);
}
  $scope.refreshPost = function (postID) {
    $http.post('/support/post/refresh/'+postID, {
     id: postID
   }).error(console.error);
    $timeout(function() {getPosts();},3000);
  };
  $scope.sendPost = function (postID) {
    if ($scope.posts[postID].searchText !== ""){
      $http.post('/support/post/answer', {
       id: postID,
      message: $scope.posts[postID].searchText
     }).error(console.error);
    } 
    $timeout(function() {$scope.refreshPost(postID);},2000);
    $scope.posts[postID].searchText= "";
    $timeout(function() {getPosts();},3000);
    
  };

  var getPosts = function () {
      // TODO mudar blueprint para método do controlador
      $http.get('/post?sort=createdAt%20DESC&state=in+progress&support=' + $scope.userInfo.id)
      .success(function (msg) {
        if (!msg) {
          console.error(msg);
          return;
        }
        $scope.posts = {};
        msg.forEach(function (post) {
          $scope.posts[post.id] = post;
        });

      })
      .error(console.error);
    };

    $scope.getReplies = function(id){
      $http.get('support/post/'+id)
      .success(function (msg) {
        if (!msg) {
          console.error(msg);
          return;
        }    
        msg.postlines.forEach(function (post) {
          $scope.replies_postLine[post.id] = post.replies;

        });
        $scope.replies_post[id] = $scope.replies_postLine;      })
      .error(console.error);

    };
    
    var subscribeToSocket = function () {
        // TODO não usar parâmetro, servidor usar o ID da sessão
        io.socket.get('/post/postUpdateSubscribe');

        io.socket.on('post', function onServerSentEvent(msg) {
          switch (msg.verb) {
            case 'addedTo':
            break;
            case 'updated':
            getPosts();
            break;
            case 'created':
            getPosts();
            break;
          }
        });

      };
      /* </MESSAGES> */
      /* <TIME> */

      var setDate = function (chats) {
        var date = new Date();
        angular.forEach($scope.chats,function(value,index){
          angular.forEach($scope.chats[index].chatLines,function(value2,index2){

            $scope.chats[index].chatLines[index2].date = 
            parseInt(((date.getTime()- new Date($scope.chats[index].chatLines[index2].date).getTime())/1000/60)
              .toFixed(0));

          })
        })
      };
      $scope.onTimeout = function(){
        var i = 0;
        if($scope.chats != null){
          var keys = Object.keys($scope.chats);

          for (i; i <= keys.length-1; i++) {
            if(angular.isDefined($scope.chats[keys[i]].chatLines[$scope.chats[keys[i]].chatLines.length-1]))
             $scope.chats[keys[i]].chatLines[$scope.chats[keys[i]].chatLines.length-1].createdAt += 1;
         }
       }
       mytimeout = $timeout($scope.onTimeout,60000);
     };

     var mytimeout = $timeout($scope.onTimeout,60000);
     /* </TIME> */
     if (!$scope.userInfo.data) {
      $http.get('/info').success(function (msg) {
        if (!msg) {
          console.error(msg);
          return;
        }

        if (msg.data) {
          $scope.userInfo = msg.data;
          subscribeToSocket();
          getPosts();
        }
      });
    } else {
    }

  }]);