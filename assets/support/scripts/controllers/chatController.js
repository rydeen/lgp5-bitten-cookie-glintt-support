'use strict';

angular.module('GlinttSupportSupport.controllers').
  controller('ChatCtrl', ['$scope', '$http', '$timeout', function ($scope, $http, $timeout) {
    $scope.userInfo = $scope.userInfo || {};
    $scope.chats = $scope.chats || {};
    $scope.unread = $scope.unread || {};
    $scope.chatsKeys = $scope.chatsKeys || [];
    $scope.activeChat = $scope.activeChat || -1;
    $scope.newMessage = "";
    $scope.messageDates = $scope.messageDates || [];
    $scope.Math = window.Math;
    $scope.agora = new Date();


    /* <CHAT> */
    $scope.resolveChat = function (customerID) {
      $scope.customer = customerID;
      $scope.newMessage = "A sua conversa foi dada como resolvida pelo assistente. Se tiver mais alguma questão, não hesite em contactar-nos.";
      $scope.sendMessage();
      $http.post('/chat/resolveChat', {
        customer: $scope.customer,
        support: $scope.userInfo.id
      }).error(console.error);

      $scope.addChat = -1;
      $scope.activeChat = -1;
    };

    $scope.sendMessage = function () {
      if ($scope.newMessage !== "" && $scope.activeChat !== -1) {
        $http.post('/chatline/newChatLine', {
          from: $scope.userInfo.username,
          chat: $scope.activeChat,
          message: $scope.newMessage
        }).error(console.error);

        $scope.newMessage = "";
      }
    };

    $scope.uploadFileDropbox = function () {

      var options = {
        success: function (files) {
          $scope.newMessage = $scope.userInfo.username + " submeteu o ficheiro <a href=\"" + files[0].link + "\">" + files[0].name + "</a>, que estará disponível nas próximas 4 horas.";
          $scope.sendMessage();
        },

        // Optional. Called when the user closes the dialog without selecting a file
        // and does not include any parameters.
        cancel: function () {

        },
        linkType: "direct",
        multiselect: false
      };

      Dropbox.choose(options);

    };

    $scope.uploadFileGoogleDrive = function () {

      // Scope to use to access user's Drive items.
      var scope = ['https://www.googleapis.com/auth/drive'];

      var pickerApiLoaded = false;
      var oauthToken;

      function onAuthApiLoad() {
        window.gapi.auth.authorize(
          {
            'client_id': clientId,
            'scope': scope,
            'immediate': false
          },
          handleAuthResult);
      }

      function onPickerApiLoad() {
        pickerApiLoaded = true;
        createPicker();
      }

      function handleAuthResult(authResult) {
        if (authResult && !authResult.error) {
          oauthToken = authResult.access_token;
          createPicker();
        }
      }

      // Create and render a Picker object for searching images.
      function createPicker() {
        if (pickerApiLoaded && oauthToken) {
          var view = new google.picker.View(google.picker.ViewId.DOCS);

          var picker = new google.picker.PickerBuilder()
            .setAppId(appId)
            .setOAuthToken(oauthToken)
            .addView(view)
            .addView(new google.picker.DocsView().setOwnedByMe(true).setIncludeFolders(true))
            .addView(new google.picker.DocsUploadView())
            .setDeveloperKey(developerKey)
            .setCallback(pickerCallback)
            .setTitle("Escolha o ficheiro para submeter")
            .setOrigin(window.location.protocol + '//' + window.location.host)
            .setLocale('pt-PT')
            .build();
          picker.setVisible(true);
        }
      }

      // A simple callback implementation.
      function pickerCallback(data) {
        if (data.action == google.picker.Action.PICKED) {
          var fileId = data.docs[0].id;

          var request = gapi.client.request({
            'path': 'https://www.googleapis.com/drive/v2/files/' + fileId,
            'method': 'GET'
          });

          request.execute(function (resp) {
            var downloadURL;

            if (typeof resp.downloadUrl === 'undefined') {
              downloadURL = resp.exportLinks['application/pdf'];

            } else downloadURL = resp.downloadUrl;

            downloadURL += '&access_token=' + oauthToken;

            $scope.newMessage = $scope.userInfo.username + " submeteu o ficheiro <a href=\"" + downloadURL + "\">" + resp.title + "</a>, que estará disponível nas próximas 8 horas.";
            $scope.sendMessage();
          });

        }
      }

      // Use the Google API Loader script to load the google.picker script.
      gapi.load('auth', {'callback': onAuthApiLoad});
      gapi.load('picker', {'callback': onPickerApiLoad});
    };


    var subscribeToSocket = function () {
      if (!io.socket.alreadyListeningToOrders) {
        io.socket.alreadyListeningToOrders = true;
        // TODO não usar parâmetro, servidor usar o ID da sessão
        io.socket.get('/chat/subscribeChatSocket', {type: 'support', id: $scope.userInfo.id});

        io.socket.on('chat', function onServerSentEvent(msg) {
          switch (msg.verb) {
            case 'addedTo':
              $http.get('/chatline/' + msg.addedId).success(function (line) {
                if (!line) {
                  console.error(line);
                  return;
                }

                if ($scope.contains($scope.chats, msg.id)) {
                  setDateMessage(line);
                  if (line.from == 'customer' && msg.id != $scope.activeChat)
                    $scope.unread[msg.id]++;
                  $scope.chats[msg.id].chatLines.push(line);
                  updateScroll();
                }
              });
              $scope.$apply();

              break;
            case 'updated':
              getMessages();
              break;
            case 'created':
              getMessages();
              break;
          }
        });
      }
    };

    $scope.activateChat = function (index) {
      $scope.activeChat = index;
      $scope.unread[index] = 0;
    };


    var getMessages = function () {
      // TODO mudar blueprint para método do controlador
      $http.get('/chat?state=in+progress&support=' + $scope.userInfo.id)
        .success(function (msg) {
          if (!msg) {
            console.error(msg);
            return;
          }

          $scope.chats = {};
          $scope.chatsKeys = [];
          msg.forEach(function (chat) {
            setDate(chat.chatLines);
            $scope.chats[chat.id] = chat;
            $scope.unread[chat.id] = 0;
            $scope.chatsKeys.push(chat.id);

          });
        })
        .error(console.error);
    };

    /* </CHAT> */
    /* <MISC> */

    var updateScroll = function () {
      var element = document.getElementById("message_box");
      element.scrollTop = 10000;
    };

    $scope.contains = function (arr, to_search) {
      var i = 0;
      var keys = Object.keys(arr);

      if (keys.length == 0)
        return false;

      for (i; i <= 1; i++) {
        if (parseInt(keys[i]) == to_search) {
          return true;
        }
      }
      return false;
    };

    /* </MISC> */
    /* <TIME FUNCTIONS> */

    var setDate = function (chatlines) {
      $scope.messageDates = [];
      var date = new Date();
      angular.forEach(chatlines, function (value2, index) {
        chatlines[index].createdAt =
          parseInt(((date.getTime() - new Date(chatlines[index].createdAt).getTime()) / 1000 / 60)
            .toFixed(0));
      })
    };

    var setDateMessage = function (message) {
      var date = new Date();
      message.createdAt =
        parseInt(((date.getTime() - new Date(message.createdAt).getTime()) / 1000 / 60)
          .toFixed(0));
    };

    $scope.onTimeout = function () {
      var i = 0;
      var keys = Object.keys($scope.chats);
      for (i; i <= keys.length - 1; i++) {
        var z = 0;
        for (z; z <= $scope.chats[keys[i]].chatLines.length - 1; z++) {
          $scope.chats[keys[i]].chatLines[z].createdAt += 1;
        }
      }
      mytimeout = $timeout($scope.onTimeout, 60000);
    };
    var mytimeout = $timeout($scope.onTimeout, 60000);

    /*</TIME FUNCTIONS> */

    if (!$scope.userInfo.data) {
      $http.get('/info')
        .success(function (msg) {
          if (!msg) {
            console.error(msg);
            return;
          }

          if (msg.data) {
            $scope.userInfo = msg.data;

            getMessages();
            subscribeToSocket();

          }
        })
        .error(console.error);
    } else {
      getMessages();
      subscribeToSocket();
    }
  }]);
