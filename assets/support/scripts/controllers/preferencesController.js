'use strict';

angular.module('GlinttSupportSupport.controllers').
  controller('PreferencesCtrl', ['$scope', '$http',
    function ($scope, $http) {
      $scope.successMessage = "";
      $scope.errorMessage = "";
      $scope.oldPassword = "";
      $scope.newPassword = "";
      $scope.newPasswordConfirm = "";


      $scope.changePassword = function () {
        $scope.successMessage = "";
        $scope.errorMessage = "";

        $http.post('/support/password', {old: $scope.oldPassword, new: $scope.newPassword})
          .success(function () {
            $scope.successMessage = "Password alterada com sucesso";
            $scope.errorMessage = "";
          })
          .error(function (msg) {
            console.error(msg);
            $scope.successMessage = "";
            $scope.errorMessage = "Houve um problema a alterar a password: ";

            if (msg.error)
              $scope.errorMessage += "\n";
          });
      };
    }]);

