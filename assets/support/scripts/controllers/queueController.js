'use strict';

angular.module('GlinttSupportSupport.controllers').
controller('QueueCtrl', ['$scope', '$http','$timeout', function ($scope, $http,$timeout) {

  $scope.userInfo = $scope.userInfo || {};
  $scope.addPost = $scope.addPost || -1;
  $scope.error_message = $scope.error_message || {};
  $scope.posts = [];
  $scope.posts_keys = [];
  $scope.Math = window.Math;

  /* <POSTS> */
  $scope.resolvePost = function (index) {
   $http.post('/support/post/solve/quick/'+index, {
     id: index,
     user: $scope.userInfo.id
   }).error(console.error);
 };

 $scope.updatePost = function (index) {
  $http.post('/support/post/accept/'+index, {
    id: index,
    user: $scope.userInfo.id
  }).error(console.error);
};

$scope.refresh = function () {
  $http.post('/support/refresh', {
  }).error(console.error);
  $timeout(function() {getPosts();},3000);
};


var getPosts = function () {
      // TODO mudar blueprint para método do controlador
      $http.get('/post?where={"state":"queue"}&sort=date')
      .success(function (msg) {
        if (!msg) {
          console.error(msg);
          return;
        }
        $scope.posts = {};
        var i = 0;
        msg.forEach(function (post) {
          $scope.posts[i] = post;
          i++;
        });
        $scope.posts_keys = Object.keys($scope.posts);
      })
      .error(console.error);

    };

    var subscribeToSocket = function () {
        // TODO não usar parâmetro, servidor usar o ID da sessão
        io.socket.get('/post/postUpdateSubscribe');

        io.socket.on('post', function onServerSentEvent(msg) {
          switch (msg.verb) {
            case 'addedTo':
            break;
            case 'updated':
            getPosts();
            break;
            case 'created':
            getPosts();
            break;
          }
        });

      };

      /* <TIME FUNCTIONS> */
      var orderByDate = function (posts) {
        var i = 0;
        var postsFinal = postsFinal || {};
        var aux = {};
        if(posts != null){
          for (i; i <= posts.length-1; i++) {
            var z=0;
            for (z; z <= posts.length-2; z++) {
              if(new Date(posts[z].postLines[0].date) > new Date(posts[z+1].postLines[0].date)){
                aux = posts[z];
                posts[z] = posts[z+1];
                posts[z+1] = aux;
              }
            }
          }
        }
        $scope.arrTest = posts;
      }
      var convertToArray = function (inputObj) {
       var output = [];
       for (var key in inputObj) {
          // must create a temp object to set the key using a variable
          var tempObj = {};
          tempObj[key] = inputObj[key];
          output.push(tempObj);
        }
        console.log(output);
      }
      
      var setDate = function (chatlines) {
        $scope.messageDates = [];
        var date = new Date();
        angular.forEach(chatlines,function(value2,index){
          chatlines[index].createdAt = 
          parseInt(((date.getTime()- new Date(chatlines[index].createdAt).getTime())/1000/60)
            .toFixed(0));
        })
      };

      var setDateMessage = function (message) {
        var date = new Date();
        message.createdAt = 
        parseInt(((date.getTime()- new Date(message.date).getTime())/1000/60)
          .toFixed(0));
      };

      $scope.onTimeout = function(){
        var i = 0;
        var keys = Object.keys($scope.posts);
        for (i; i <= keys.length-1; i++) {
          var z = 0;
          for (z; z <= $scope.posts[keys[i]].postLines.length-1; z++) {
           $scope.posts[keys[i]].postLines[z].createdAt += 1;
         }
       }
       mytimeout = $timeout($scope.onTimeout,60000);
     };
     var mytimeout = $timeout($scope.onTimeout,60000);

     /*</TIME FUNCTIONS> */
     if (!$scope.userInfo.data) {
      $http.get('/info').success(function (msg) {
        if (!msg) {
          console.error(msg);
          return;
        }

        if (msg.data) {
          $scope.userInfo = msg.data;
          subscribeToSocket();
          getPosts();
        }
      });
    } else {
    }

  }]);

