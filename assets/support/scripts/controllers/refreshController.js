'use strict';

angular.module('GlinttSupportSupport.controllers').
  controller('RefreshCtrl', ['$scope', '$http', '$timeout', '$location',
    function ($scope, $http, $timeout, $location) {

      $scope.userInfo = $scope.userInfo || {};
      $scope.addChat = $scope.addChat || -1;
      $scope.error_message = $scope.error_message || {};
      $scope.chats = $scope.chats || {};

      /* <MESSAGES> */

      $scope.updateChat = function (index) {
        $scope.addChat = index;

        $http.post('/chat/addChat', {
          customer: $scope.addChat,
          support: $scope.userInfo.id,
          date: new Date()
        }).error(console.error);

        $scope.addChat = -1;
      };

      $scope.resolveChat = function (index) {
        $scope.addChat = index;

        $http.post('/chat/resolveChat', {
          customer: $scope.addChat,
          support: $scope.userInfo.id,
          date: new Date()
        }).error(console.error);

        $scope.addChat = -1;
      };

      var subscribeToSocket = function () {
        // TODO não usar parâmetro, servidor usar o ID da sessão
        io.socket.get('/chat/chatUpdateSubscribe');

        io.socket.on('chat', function onServerSentEvent(msg) {
          getMessages();
        });

      };

      var getMessages = function () {
        // TODO mudar blueprint para método do controlador
        $http.get('/chat?sort=createdAt%20DESC&state=queue')
          .success(function (msg) {
            if (!msg) {
              console.error(msg);
              return;
            }
            $scope.chats = {};
            $scope.chatsKeys = [];
            msg.forEach(function (chat) {
              $scope.chats[chat.id] = chat;
            });
            setDate($scope.chats);
          })
          .error(console.error);
      };

      /* </MESSAGES> */
      /* <TIME> */

      var setDate = function (chats) {
        var date = new Date();
        angular.forEach($scope.chats, function (value, index) {
          angular.forEach($scope.chats[index].chatLines, function (value2, index2) {

            $scope.chats[index].chatLines[index2].createdAt =
              parseInt(((date.getTime() - new Date($scope.chats[index].chatLines[index2].createdAt).getTime()) / 1000 / 60)
                .toFixed(0));

          })
        })
      };
      $scope.onTimeout = function () {
        var i = 0;
        var keys = Object.keys($scope.chats);

        for (i; i <= keys.length - 1; i++) {
          if (angular.isDefined($scope.chats[keys[i]].chatLines[$scope.chats[keys[i]].chatLines.length - 1]))
            $scope.chats[keys[i]].chatLines[$scope.chats[keys[i]].chatLines.length - 1].createdAt += 1;
        }

        mytimeout = $timeout($scope.onTimeout, 60000);
      };

      var mytimeout = $timeout($scope.onTimeout, 60000);
      /* </TIME> */
      if (!$scope.userInfo.data) {
        $http.get('/info').success(function (msg) {
          if (!msg) {
            console.error(msg);
            return;
          }

          if (msg.data) {
            $scope.userInfo = msg.data;
            subscribeToSocket();
            getMessages();
          }
        });
      } else {
      }
    }]);
