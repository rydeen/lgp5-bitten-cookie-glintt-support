'use strict';

angular.module('GlinttSupportSupport', [
  'ngRoute',
  'ngSanitize',
  'GlinttSupportSupport.controllers'
]).
  config(['$routeProvider', function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'support/views/index.html',
        resolve: {
          loggedin: function (loggedInService) {
            return loggedInService.checkLoggedIn();
          }
        }
      })
      .when('/preferences', {
        templateUrl: 'support/views/preferences.html',
        resolve: {
          loggedin: function (loggedInService) {
            return loggedInService.checkLoggedIn();
          }
        }
      })
      .otherwise({redirectTo: '/'});
  }])
  .controller('LogoutCtrl', ['$scope', '$http', '$location',
    function ($scope, $http, $location) {

      $scope.logout = function () {
        $http.get('/logout').success(function () {
          $location.path('/loginpage');
        })
          .error(console.error);
      };

      $scope.userInfo = $scope.userInfo || {};

      var getUserInfo = function () {
        $http.get('/info').success(function (msg) {
          if (!msg) {
            console.error(msg);
            return;
          }

          if (msg.data)
            $scope.userInfo = msg.data;
        });
      };

      if (!$scope.userInfo.data)
        getUserInfo();

    }]);

angular.module('GlinttSupportSupport.controllers', []);
