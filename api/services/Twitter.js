module.exports = {

	getAppAccess : function(){
		var Twit = require('twit')

		var T = new Twit({
		  consumer_key: sails.config.keys.TWITTER_CONSUMER_KEY,
		  consumer_secret: sails.config.keys.TWITTER_CONSUMER_SECRET,
		  access_token: sails.config.keys.TWITTER_ACCESS_TOKEN,
		  access_token_secret: sails.config.keys.TWITTER_ACCESS_TOKEN_SECRET
		});

		return T;
	},

	fetchHashtag: function(word,callback){
		
		var T = require('twit');
		T = sails.services.twitter.getAppAccess();

        var requestedword = word;
        T.get('search/tweets', { q: '#' + requestedword}, function (err, data, response) {
            if(err) sails.log.error(err);
            else{
            	// Filtrar a informação que precisamos
            	if(data){
            		//sails.log.debug("\t Found " + Object.keys(data).length + " tweets.");
            		var tweets = new Array();
            		//console.log("fecth: " + word);
            		async.each(data.statuses,
		                function(curr,cb){
		                    var currentTweet = {
		                    	tweetID: curr.id_str,
		                    	text: curr.text,
		                    	user: {
		                    		name: curr.user.name,
		                    		username: curr.user.screen_name,
		                    		location: curr.user.location,
		                    		image: curr.user.profile_image_url.replace("_normal", ""),
		                    	},
		                    	date: curr.created_at
		                    }
							tweets.push(currentTweet);
							cb();

		                },
		                function(err){
		                	// Depois de todos os elementos serem processados entra aqui
		                	// Erro numa das chamadas acima
		                	if(err) sails.log.error(err);
		                	else{
		                		// Retornar aqui era fixe mas callbacks nao me deixam vir buscar isto porque assincrono
		                		//sails.log.debug(tweets);
		                		callback(tweets);
		                		//return tweets;
		                	}
		                });
            	}
            	else{
            		sails.log.debug("There is no data");
            	}

            }
        })
	},

	replyPost: function(post, in_reply_to, message, callback){
		var T = require('twit');
		T = sails.services.twitter.getAppAccess();

		var msg = "@" + in_reply_to + " " + message;
		T.post('statuses/update',
			{ 
				status: msg,
				in_reply_to_status_id: post
			},
			function(err, data, response) {
		  		if(err){
		  			callback(err);
		  		}
		  		else{
		  			var tweet = data;
		  			var currentTweet = {
                    	tweetID: tweet.id_str,
                    	text: tweet.text,
                    	user: {
                    		name: tweet.user.name,
                    		username: tweet.user.screen_name,
                    		photoURL: tweet.user.profile_image_url.replace("_normal", ""),
                    	},
                    	date: tweet.created_at
                    }

                    callback(currentTweet);
		  		}
			});

		
	},

	fetchPost: function(socialID, callback){
		var T = require('twit');
		T = sails.services.twitter.getAppAccess();

		T.get('statuses/show/' + socialID,
			function(err, data, response) {
				console.log(data);
		  		if(err){
		  			sails.log.error(err);
		  			callback(null);
		  		}
		  		else{
		  			var tweet = data;
		  			var currentTweet = {
                    	tweetID: tweet.id_str,
                    	text: tweet.text,
                    	user: {
                    		name: tweet.user.name,
                    		username: tweet.user.screen_name,
                    		photoURL: tweet.profile_image_url.replace("_normal", ""),
                    	},
                    	date: tweet.created_at
                    }

                    callback(currentTweet);
		  		}
			}
		);
	},


}