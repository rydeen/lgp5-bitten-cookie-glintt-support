module.exports = {

	fetchPosts : function(){

		sails.log.debug("Retrieving data from Social Networks.");
        //sails.log.debug("Retrieving important Hashtags from database.");
        
        var tweetsFetched = new Array();
        //var twittercounter = 0;
        
        Hashtag.find(function(err,data){
            //console.log(data);
            if(data){
                async.each(data,
                    function(curr,cb){
                        //sails.log.debug("\t Searching Twitter for: " + curr.hashtag);
                        var tweets = sails.services.twitter.fetchHashtag(curr.hashtag, 
                            function(res){
                                // Executa quando acaba de fazer fetch das Hashtags
                                tweetsFetched.push(res);
                                cb(); 
                            });
                                       
                    },
                    function(err){
                        // Depois de todos os elementos serem processados entra aqui
                        // Se houver erro numa das chamadas acima
                        if(err) { 
                            //sails.log.error("DEPOIS DE FAZER FETCH")
                            sails.log.error(err);
                        }
                        else{
                            // Meter num só array todos os tweets
                            var tweets = [].concat.apply([], tweetsFetched);
                            
                            tweets.forEach(function(item){
                                if(item.tweetID){
                                	// Inserir na DB se ainda não existe o Tweet que foi fetched
                                	
                                	var newPostLine = {
                                		socialID: item.tweetID,
                                		message: item.text,
                                		author: item.user.name,
                                		username: item.user.username,
                                		photoURL: item.user.image,
                                		date: new Date(item.date)
                                	};

                                    // Se não existe esta linha na db, então é criado um Post para associar a essa linha

                                    PostLine.findOne({socialID: item.tweetID}, 
                                        function(err,postline){
                                            if(err){
                                                //sails.log.error("A PROCURAR POSTLINE")
                                                sails.log.error(err);
                                            }
                                            if(!postline){
                                                // Nao existe postline
                                                var newPost = {
                                                    state: 'queue',
                                                    socialNetwork: 'twitter',
                                                    date: newPostLine.date
                                                };

                                                Post.create(newPost, function(err,post){
                                                    if(err){
                                                        //sails.log.error("A CRIAR POST")
                                                        sails.log.error(err);
                                                    }
                                                    if(post){
                                                        // foi criado post
                                                        // Criar indicador de que foi inserido na Queue
                                                        Post.queuePost(post.id);
                                                        newPostLine.post = post.id;
                                                        PostLine.create(newPostLine, function(err, postline){
                                                            if(err){
                                                                //sails.log.error("A CRIAR POSTLINE")
                                                                sails.log.error(err);
                                                            }
                                                            //console.log("criado postline " + newPostLine.socialID + " ficou com post " + post.id);
                                                        });
                                                    }
                                                });

                                            }
                                            else{
                                                // Post já foi pescado das redes sociais, não ha necessidade de tratar.
                                            }
                                            
                                        });
                                }

                                //twittercounter++;	
                            })

                            sails.log.debug("Twitter data retrieved with success.");
                            

                        }
                    });
            }
            else{
                sails.log.debug("Database has no hashtags defined for Twitter searches.");
            }
        });
        

        var facebookPostsFetched = new Array();
        //var fbcounter = 0;

        sails.services.facebook.fetchPagePosts(
            function(fbData){

                if(fbData){

                    async.each(fbData,
                        function(curr,cb){
                            var pageManagerID = sails.services.facebook.getFacebookPageID();
                            // Se quem postou foi o criador da pagina nao vai aparecer no feed
                            if(curr.from.id != pageManagerID){
                                // Ver se já existe este postline na db
                                // Se não existe é criado Post se já existe vao ser procurados novas linhas e replies possiveis(já tá na queue)
                                // Eventualmente para reforçar, pode-se fazer verificar se o post associado não esta resolvido
                                var newPostLine = {
                                        socialID: curr.id,
                                        message: curr.message,
                                        author: curr.from.name,
                                        username: curr.from.name,
                                        photoURL: "http://graph.facebook.com/" + curr.from.id + "/picture?type=square&height=200&width=200",
                                        date: new Date(curr.created_time)
                                    };

                                PostLine.findOne({socialID: curr.id},
                                    function(err,postline){
                                        if(err){
                                            sails.log.error("Erro a procurar Postline na Base de Dados")
                                            sails.log.error(err);
                                        }
                                        if(!postline){
                                            // Nao existe é preciso criar um post e adicionar as linhas
                                            var newPost = {
                                                    state: 'queue',
                                                    socialNetwork: 'facebook',
                                                    date: newPostLine.date
                                                };

                                            Post.create(newPost, function(err,post){
                                                if(err){
                                                    sails.log.error("Erro a criar novo Post")
                                                    sails.log.error(err);
                                                }
                                                if(post){
                                                    // foi criado post
                                                    //sails.log.debug("created post with id " + post.id + " and SN " + post.socialNetwork);
                                                    Post.publishCreate({id:post.id,socialNetwork:post.socialNetwork});
                                                    newPostLine.post = post.id;
                                                    PostLine.create(newPostLine, function(err, postline){
                                                        if(err){
                                                            sails.log.error(err);
                                                        }
                                                        //console.log("criado postline " + newPostLine.socialID + " ficou com post " + post.id);
                                                    });

                                                    // Criar indicador de que foi inserido na Queue
                                                    Post.queuePost(post.id);

                                                    if(curr.comments){
                                                        async.each(curr.comments.data,
                                                            function(curr,cb){
                                                                // A cada comentario
                                                                var commentLine = {
                                                                    socialID: curr.id,
                                                                    message: curr.message,
                                                                    author: curr.from.name,
                                                                    username: curr.from.name,
                                                                    photoURL: "http://graph.facebook.com/" + curr.from.id + "/picture?type=square&height=200&width=200",
                                                                    date: new Date(curr.created_time),
                                                                    post: post.id
                                                                };
        //
                                                                PostLine.create(commentLine, function(err, commentLine){
                                                                    if(err){
                                                                        sails.log.error("Erro a adicionar comentário de uma publicação à base de dados")
                                                                        sails.log.error(err);
                                                                    }
                                                                    else if(commentLine){
                                                                       var replies = sails.services.facebook.fetchReplies(commentLine.socialID,
                                                                            function(replyData){
                                                                                async.each(replyData,
                                                                                    function(curr1,cb){
                                                                                        var postReply = {
                                                                                            socialID: curr1.id,
                                                                                            message: curr1.message,
                                                                                            author: curr1.from.name,
                                                                                            username: curr1.from.name,
                                                                                            photoURL: "http://graph.facebook.com/" + curr1.from.id + "/picture?type=square&height=200&width=200",
                                                                                            date: new Date(curr1.created_time),
                                                                                            post: post.id,
                                                                                            postline: commentLine.id
                                                                                        };


                                                                                        PostReply.findOrCreate({socialID: curr1.id}, postReply, function(err, replyLine){
                                                                                            
                                                                                            if(err){
                                                                                                sails.log.error("Erro a adicionar uma resposta de uma publicação à base de dados")
                                                                                                sails.log.error(err);
                                                                                            }
                                                                                            else if(replyLine){
                                                                                                //sails.log.debug("adicionado reply -"+ curr1.message + "- a -" + commentLine.message);
                                                                                            }

                                                                                        });


                                                                                    },
                                                                                    function(err){
                                                                                        if(err){
                                                                                            sails.log.error("Erro a adicionar replies de um comentario à base de dados")
                                                                                            sails.log.error(err);
                                                                                        }
                                                                                    });
                                                                            });
                                                                    }
                                                                    
                                                                });

                                                                cb();
        //
                                                            },
                                                            function(err){
                                                                // Depois de percorrer todos os comentarios
                                                                if(err){
                                                                    sails.log.error(err);
                                                                }
                                                            });
                                                        
                                                    }
                                                }

                                            });

                                        }
                                        else{
                                            // Ja existe a Postline, mas podem nao estar guardados todos os comentarios ou replies a comentarios
                                            if(curr.comments){
                                                Post.countLines(postline.post,
                                                    function(err, linesInDB){
                                                        if(err){ sails.log.error(err); }    

                                                        if(linesInDB != curr.comments.data.length + 1){
                                                            //console.log("Requeue por novo/remocao Postline");
                                                            Post.reQueuePost(postline.post);
                                                        }   

                                                        async.each(curr.comments.data,
                                                        function(comment,cb){
                                                            // A cada comentário
                                                            var newPostline = {
                                                                socialID: comment.id,
                                                                message: comment.message,
                                                                author: comment.from.name,
                                                                username: comment.from.name,
                                                                photoURL: "http://graph.facebook.com/" + comment.from.id + "/picture?type=square&height=200&width=200",
                                                                date: new Date(comment.created_time),
                                                                post: postline.post
                                                            };

                                                            PostLine.findOrCreate({socialID: comment.id}, newPostline, function(err, commentLine){
                                                                if(err){
                                                                    sails.log.error("Erro a adicionar comentário de uma publicação à base de dados")
                                                                    sails.log.error(err);
                                                                }
                                                                else{
                                                                    var replies = sails.services.facebook.fetchReplies(commentLine.socialID,
                                                                        function(replyData){         
                                                                            PostLine.countReplies(commentLine.id,
                                                                                function(err, repliesInDB){
                                                                                    if(err) { sails.log.error(err); }
                                                                                    if(repliesInDB != replyData.length){
                                                                                        Post.reQueuePost(postline.post);
                                                                                    }
                                                                                })

                                                                            async.each(replyData,
                                                                                function(curr1,cb){
                                                                                    var postReply = {
                                                                                        socialID: curr1.id,
                                                                                        message: curr1.message,
                                                                                        author: curr1.from.name,
                                                                                        username: curr1.from.name,
                                                                                        photoURL: "http://graph.facebook.com/" + curr1.from.id + "/picture?type=square&height=200&width=200",
                                                                                        date: new Date(curr1.created_time),
                                                                                        post: postline.post,
                                                                                        postline: commentLine.id
                                                                                    };

                                                                                    

                                                                                    PostReply.findOrCreate({socialID: curr1.id}, postReply, function(err, replyLine){
                                                                                        
                                                                                        if(err){
                                                                                            sails.log.error("Erro a adicionar uma resposta de uma publicação à base de dados")
                                                                                            sails.log.error(err);
                                                                                        }
                                                                                        else if(replyLine){
                                                                                            cb()
                                                                                            //sails.log.debug("adicionado reply -"+ curr1.message + "- a -" + commentLine.message);
                                                                                        }

                                                                                    });


                                                                                },
                                                                                function(err){
                                                                                    if(err){
                                                                                        sails.log.error("Erro a adicionar replies de um comentario à base de dados")
                                                                                        sails.log.error(err);
                                                                                    }
                                                                                });
                                                                        }
                                                                    );
                                                                }
                                                            });
                                                            cb();

                                                        },
                                                        function(err){
                                                            // No final de todos os comentarios do postline "curr
                                                        });
                                                    }
                                                );
                                            }
                                        }
                                    }

                                );

                            }

                            cb();
                        },
                        function(err){
                            if(err){
                                sails.log.error("Erro a processar dados do Facebook");
                                sails.log.error(err);                        
                            }
                            //console.log("Corro isto no fim de percorrer dada fbdata");
                            sails.log.debug("Facebook data retrieved with success.");
                        });
                    }
                }
                
            

        );

        // para o refresh
        return true;


	}

}