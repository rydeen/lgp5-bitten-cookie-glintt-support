module.exports = {

	getAppAccess : function(){
		var FB = require('fb');

		/* 
			!IMPLEMENTAÇÃO
			Esta linha vai depender de hospital e da sua página
			Deve existir uma aplicação associada à página a gerir
			(http://stackoverflow.com/questions/9265062/how-to-link-a-facebook-app-with-an-existing-fan-page)

			Só consegui obter o access token da app (com pagina já ligada) em
			//http://nepalonrails.com/blog/2013/03/how-to-get-facebook-page_id-and-access_token-to-publish-on-its-wall - falta o tic no publish_pages
	 		// Este token tem que ser gerado a partir de uma conta com permissoes de administração sobre a app.
			Esse token deve ficar em baixo e não expira (https://developers.facebook.com/docs/facebook-login/access-tokens)
			
			Logo é aconselhado que esta informação seja guardada na máquina e não no código
		*/
		// A key abaixo não tem permissões de escrita
	 	//var app_access_token = '765030603609869' + '|' + 'vMJPvje262n8QFN4C6TTXg8DxME';

	 	// Para ter permissões de publicar no Facebook em nome da Página:
	 	// http://stackoverflow.com/questions/10183625/extending-facebook-page-access-token
	 	//var app_access_token = 'CAAK3ypFe1w0BADZCYgfYn9NMwUkoOLcYZBvejca6iHZBlnfIXGJpIUyDE1JrHcswyVUgaAIQVJHEMHxKZBQWUP8m4YpLrBnPb6K49GGjSFZA4kIB8oZCMUkiZCKUNnhcGNwHXU4IVSItiG8qSK8ipZBrUlUGunaYgydShO4crv8wYvO2cHcj9UhAlSj6fNYrZBN47cW4iZCdOFLLlQ1eX9klEq';
	 	
	    //FB.setAccessToken(app_access_token);
	    FB.setAccessToken(sails.config.keys.FACEBOOK_APP_ACCESS_TOKEN);
	    return FB;
	},

	getFacebookPageID : function(){
		/* !IMPLEMENTAÇÃO
		 	Varia consoante a pagina do hospital			
			O id usado abaixo é de uma app teste com uma pagina associada. 	
		*/
		return sails.config.keys.FACEBOOK_PAGE_ID;
	    //return '562715663868404';
	},

	fetchPagePosts : function(callback) {
		// vai ter de ficar asincrono prolly
		var pageID = sails.services.facebook.getFacebookPageID();
		sails.services.facebook.getAppAccess();	

		var FB= require('fb');
		FB.api(pageID + '/feed/', 
	    	{
	    		include_hidden : false
	    	}, 
	    	function (result) {
	    		if(result.data){
	    			callback(result.data);
	    		}
	    		else{
	    			sails.log.error("Error retrieving data from Facebook");
	    			sails.log.error(result);
	    		}
	   	    });
	},

	fetchReplies : function(commentID, callback) {
		// vai ter de ficar asincrono prolly
		var pageID = sails.services.facebook.getFacebookPageID();
		sails.services.facebook.getAppAccess();	

		var FB= require('fb');
		FB.api(commentID + '/comments/', 
	    	{
	    		order : "chronological"
	    	}, 
	    	function (result) {
	    		if(result.data){
	    			callback(result.data);
	    		}
	    		else{
	    			sails.log.error("Error retrieving reply data from Facebook");
	    			sails.log.error(result);
	    		}
	   	    });
	},

	fetchPost: function(socialID, callback){
		var pageID = sails.services.facebook.getFacebookPageID();
		sails.services.facebook.getAppAccess();	

		var FB= require('fb');
		FB.api(socialID, 
	    	{
	    		order : "chronological"
	    	}, 
	    	function (result) {
	    		if(result){
    				callback(result);
	    		}
	    		else{
	    			sails.log.error("Error retrieving post data from Facebook");
	    			sails.log.error(result);
	    			callback(null);
	    		}
	   	    });
	},

	replyPost: function(post, message, callback){
		var FB = require('fb');
		var pageID = sails.services.facebook.getFacebookPageID();
		sails.services.facebook.getAppAccess();	

		FB.api(post + '/comments', 'post', {message: message}, function (res) {

			if(!res || res.error) {
				sails.log.error(res.error);
				callback(res.error);
			}
				callback(null);
		});
	}
}