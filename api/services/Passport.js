var passport = require('passport'),
  LocalStrategy = require('passport-local').Strategy,
  FacebookStrategy = require('passport-facebook').Strategy,
  TwitterStrategy = require('passport-twitter').Strategy,
  GoogleStrategy = require('passport-google-oauth').OAuth2Strategy,
  bcrypt = require('bcrypt-nodejs');
//helper functions
function findById(id, fn) {
  if (id > 1000000) { // distinguir de facebook id/ twitterid / googleid. sim, trolha
    CustomerUser.find({socialNetworkID: id}, function (err, user) {
      if (err) {
        return fn(null, null);
      } else {
        //console.log("encontrou customer");
        var returnUser = {
          username: user[0].username,
          id: user[0].id,
          socialNetworkID: user[0].socialNetworkID,
          socialNetwork: user[0].socialNetwork,
          permissions: "client",
          photoURL: user[0].photoURL,
          birthday: user[0].birthday,
          email: user[0].birthday
        };
        return fn(null, returnUser);
      }
    });
  }
  else {
    SupervisorUser.findOne(id, function (err, user) {
      //console.log("procurar supervisor");
      if (err) {
        return fn(null, null);
      } 
      else if (!user) {
          // É support
          SupportUser.findOne(id, function (err, suser) {

            // Error handling
            if (err) {
              return fn(null, null);
            } 
            else if(suser){
              suser.permissions = "support";
              return fn(null, suser);
            }
          });
        }
        else {
          //console.log("encontrou supervisor");

          user.permissions = "supervisor";
          return fn(null, user);
        }
      });
    }
}

function findByUsername(u, fn) {
  SupervisorUser.findOne({
    username: u
  }, function (err, user) {
    // Error handling
    if (err) {
      return fn(null, null);
      // The User was found successfully!
    } else {
      if (!user) {
        SupportUser.findOne({
          username: u
        }, function (err, user) {
          // Error handling
          if (!user || err) {
            return fn(null, null);
            // The User was found successfully!
          } else {
            user.permissions = "support";
            return fn(null, user);
          }
        });
      }
      else {
        user.permissions = "supervisor";
        return fn(null, user);
      }
    }
  });
}

// Passport session setup.
// To support persistent login sessions, Passport needs to be able to
// serialize users into and deserialize users out of the session. Typically,
// this will be as simple as storing the user ID when serializing, and finding
// the user by ID when deserializing.
passport.serializeUser(function (user, done) {
  //console.log("before serialize");
  //console.log(user);

  if (user.socialNetwork) {
    done(null, user.socialNetworkID)
  }
  else {
    done(null, user.id);
  }
});

passport.deserializeUser(function (id, done) {
  findById(id, function (err, user) {
    done(err, user);
  });
});

// Use the LocalStrategy within Passport.
// Strategies in passport require a `verify` function, which accept
// credentials (in this case, a username and password), and invoke a callback
// with a user object.
passport.use(new LocalStrategy(
  function (username, password, done) {
    // asynchronous verification, for effect...
    process.nextTick(function () {
      // Find the user by username. If there is no user with the given
      // username, or the password is not correct, set the user to `false` to
      // indicate failure and set a flash message. Otherwise, return the
      // authenticated `user`.
      findByUsername(username, function (err, user) {
        if (err)
          return done(null, err);
        if (!user) {
          return done(null, false, {
            message: 'Unknown user ' + username
          });
        }
        bcrypt.compare(password, user.password, function (err, res) {
          if (!res)
            return done(null, false, {
              message: 'Invalid Password'
            });

          return done(null, user, {
            message: 'Logged In Successfully'
          });
        });
      })
    });
  }
));


passport.use(new FacebookStrategy({
    clientID: sails.config.keys.FACEBOOK_APP_CLIENT_ID,
    clientSecret: sails.config.keys.FACEBOOK_APP_SECRET,
    callbackURL: "/client/login/facebook/callback"
  },
  function (accessToken, refreshToken, profile, done) {
    var user = {};

    // Se a conta Facebook recebida tem o Aniversário do Utilizador acessível, a aplicação vai guardar essa informação
    if(profile._json.birthday){
      user = {
        username: profile.displayName,
        socialNetworkID: profile.id,
        socialNetwork: "facebook",
        email: profile._json.email,
        birthday: new Date(profile._json.birthday),
        photoURL: "http://graph.facebook.com/" + profile.id + "/picture?type=square&height=200&width=200"
      } 
    }
    else{
      user = {
        username: profile.displayName,
        socialNetworkID: profile.id,
        socialNetwork: "facebook",
        email: profile._json.email,
        photoURL: "http://graph.facebook.com/" + profile.id + "/picture?type=square&height=200&width=200"
      } 
    }

    //console.log(user);

    CustomerUser.findOrCreate(
      {
        socialNetwork: "facebook",
        socialNetworkID: profile.id
      },
      user
      , function (err, user) {
        if (err) {
          return done(err);
        }
        if (user) {

          var returnUser = {
            username: user.username,
            createdAt: user.createdAt,
            id: user.id,
            permissions: "client",
            socialNetworkID: user.socialNetworkID,
            socialNetwork: "facebook",
            email: user.email,
            birthday: user.birthday,
            photoURL: user.photoURL
          };
          //console.log(founduser);

        }

        done(null, returnUser);
      });

  }
));


passport.use(new TwitterStrategy({
    consumerKey: sails.config.keys.TWITTER_CONSUMER_KEY,
    consumerSecret: sails.config.keys.TWITTER_CONSUMER_SECRET,
    callbackURL: "/client/login/twitter/callback"
  },
  function (token, tokenSecret, profile, done) {
    var user = {
      username: profile.displayName,
      socialNetworkID: profile.id,
      socialNetwork: "twitter",
      photoURL: profile._json.profile_image_url.replace("_normal", "")
    };

    CustomerUser.findOrCreate(
      {
        socialNetwork: "twitter",
        socialNetworkID: profile.id
      },
      user,
      function (err, user) {
        if (err) {
          return done(err);
        }
        if (user) {
          var returnUser = {
            username: user.username,
            createdAt: user.createdAt,
            id: user.id,
            permissions: "client",
            socialNetworkID: profile.id,
            socialNetwork: "twitter",
            photoURL: user.photoURL
          };
        }
        done(null, returnUser);
      }
    );
  }
));

passport.use(new GoogleStrategy({
    clientID: sails.config.keys.GOOGLE_CLIENT_ID,
    clientSecret: sails.config.keys.GOOGLE_CLIENT_SECRET,
    callbackURL: '/client/login/google/callback'
  },


  function (token, tokenSecret, profile, done) {

    var urlpic = profile._json.image.url.replace("sz=50", "sz=200");
    var user = {};

    if(profile._json.birthday){
      user = {
        username: profile.displayName,
        socialNetworkID: profile.id,
        socialNetwork: "google",
        email: profile.emails[0].value,
        birthday: new Date(profile._json.birthday),
        photoURL: urlpic
      };
    }
    else{
      user = {
        username: profile.displayName,
        socialNetworkID: profile.id,
        socialNetwork: "google",
        email: profile.emails[0].value,
        photoURL: urlpic
      };
    }

    CustomerUser.findOrCreate(
      {
        socialNetwork: "google",
        socialNetworkID: profile.id
      },
      user,
      function (err, user) {
        if(err){
          sails.log.error(err);
          done(err);
        }
        if(user){
          var returnUser = {
            username: user.username,
            createdAt: user.createdAt,
            permissions: "client",
            socialNetworkID: profile.id,
            socialNetwork: "google",
            photoURL: user.photoURL
          };
          done(err, returnUser);
        }
        
      });
  }
));
