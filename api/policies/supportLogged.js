/**
 * Logged In
 *
 * @module      :: Policy
 * @description :: Simple policy to allow any authenticated user
 *                 Assumes that your login action in one of your controllers sets `req.session.authenticated = true;`
 * @docs        :: http://sailsjs.org/#!documentation/policies
 *
 */
module.exports = function (req, res, next) {

  // Se não está logado lança erro
  if (!req.isAuthenticated()) {
    return res.error(req.__("user_not_logged_error"), req.__("user_not_logged_error_code"));
  }

  if (req.session.permissions !== "support" && req.session.permissions !== "supervisor") {
    return res.error(req.__("permission_error"), req.__("permission_error_code"))
  }

  return next();
};
