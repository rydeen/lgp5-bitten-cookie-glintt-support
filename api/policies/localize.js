// api/policies/localize.js
module.exports = function(req, res, next) {
	req.locale = 'en';
	next();
};