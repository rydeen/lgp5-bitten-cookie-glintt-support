/**
 * Logged In
 *
 * @module      :: Policy
 * @description :: Policy to allow Client authenticated user
 * @docs        :: http://sailsjs.org/#!documentation/policies
 *
 */
module.exports = function(req, res, next) {

	// Se não está logado lança erro
	
	if ( !req.isAuthenticated() ){
		return res.error(req.__("user_not_logged_error") , req.__("user_not_logged_error_code"));
	}
	else if( req.session.permissions != "client"){
		return res.error(req.__("permission_error") , req.__("permission_error_code"))
	}
	
	
	return next();
};
