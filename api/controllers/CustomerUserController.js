/**
 * CustomerUserController
 *
 * @description :: Server-side logic for managing Customerusers
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var passport = require('passport');

module.exports = {

  logout: function (req, res) {
    req.logout();
    req.session.destroy();
    //res.redirect('/loginpage');
    return res.success(req.__("user_logout_success"), req.__("user_logout_success_code"));
  },

  info: function (req, res) {
    if(req.user){
      if(req.user.password){
        delete req.user.password;
        return res.success(req.__("user_data_sucess"), req.__("user_data_sucess_code"), req.user);
      }
      else {
        return res.success(req.__("user_data_sucess"), req.__("user_data_sucess_code"), req.user);
      }
    }
    else return res.error(req.__("user_not_logged_error"), req.__("user_not_logged_error_code"));
  }
};

