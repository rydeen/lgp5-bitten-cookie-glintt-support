/**
 * PostController
 *
 * @description :: Server-side logic for managing Posts
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

  getThisYear: function (req, res) {
    Post.find({createdAt: {'>': new Date('1/1/' + new Date().getFullYear())}})
      .populate('exitedQueue')
      .populate('solvedDatetime')
      .populate('enteredQueue')
      .exec(function (err, posts) {
        if (err) {
          sails.log.error(err);

          return res.error(req.__("this_year_posts_error"), req.__("this_year_posts_error_code"));
        }

        res.send(posts);
      });
  },

  accept: function (req, res) {
    if (!req.params.id) {
      return res.error(req.__('missing_parameter_error'), req.__('missing_parameter_error_code'))
    }

    var loggedID = req.user.id;
    var acceptedPost = req.params.id;

    //SupportUser.find(id: loggedID, function(){})

    Post.findOne({id: acceptedPost, state: 'queue'}, function (err, post) {
      if (err) {
        sails.log.error(err);
        return res.error(req.__('missing_post'), req.__('missing_post_code'))
      }
      else if (post) {
        // Update no post
        Post.update(
          {
            id: acceptedPost,
            state: 'queue'
          },
          {
            state: 'in progress',
            support: loggedID,
          },
          function (err, posts) {
            if (err) {
              sails.log.error(err);
            }
            else if (posts.length != 0) {
              Post.publishUpdate(acceptedPost);
              Post.updateExitedQueue(posts[0].id, loggedID);
              return res.success(req.__('post_assignment_sucess'), req.__('post_assignment_sucess_code'), posts[0]);
            }
            else {
              return res.error(req.__('missing_post'), req.__('missing_post_code'))
            }

          });
      }
      else {
        return res.error(req.__('missing_post'), req.__('missing_post_code'))
      }

    });
  },

  solve: function (req, res) {
    if (!req.params.id) {
      return res.error(req.__('missing_parameter_error'), req.__('missing_parameter_error_code'))
    }
    var loggedID = req.user.id;
    var acceptedPost = req.params.id;

    Post.findOne({id: acceptedPost, support: loggedID, state: 'in progress'}, function (err, post) {
      if (err) {
        return res.error(req.__('missing_post_assign'), req.__('missing_post_assign_code'))
      }
      else if (post) {
        Post.update(
          {
            id: acceptedPost,
            support: loggedID,
            state: 'in progress'
          },
          {
            state: 'solved',
          },
          function (err, posts) {
            if (err) {
              sails.log.error(err);
            }
            else if (posts.length != 0) {
              Post.publishUpdate(acceptedPost);
              Post.updateSolvedTime(posts[0].id, loggedID);
              return res.success(req.__('post_solve_sucess'), req.__('post_solve_sucess_code'), posts[0]);
            }
            else {
              return res.error(req.__('missing_post_assign'), req.__('missing_post_assign_code'))
            }

          });
      }
      else {
        return res.error(req.__('missing_post_assign'), req.__('missing_post_assign_code'))
      }

    });


  },

  quicksolve: function (req, res) {
    if (!req.params.id) {
      return res.error(req.__('missing_parameter_error'), req.__('missing_parameter_error_code'))
    }
    sails.log.debug("userid: " + req.user.id + " acceptedPost: " + req.params.id);
    var loggedID = req.user.id;
    var acceptedPost = req.params.id;

    Post.findOne({id: acceptedPost, state: 'queue'}, function (err, post) {
      if (err) {
        return res.error(req.__('missing_post'), req.__('missing_post_code'))
      }
      else if (post) {
        Post.update(
          {
            id: acceptedPost,
            state: 'queue'
          },
          {
            state: 'solved',
            support: loggedID,
          },
          function (err, posts) {
            if (err) {
              sails.log.error(err);
            }
            else if (posts.length != 0) {
              Post.publishUpdate(acceptedPost);
              Post.updateSolvedTime(posts[0].id, loggedID);
              Post.updateExitedQueue(posts[0].id, loggedID);
              return res.success(req.__('post_solve_sucess'), req.__('post_solve_sucess_code'), posts[0]);
            }
            else {
              return res.error(req.__('missing_post'), req.__('missing_post_code'))
            }

          });
      }
      else {
        return res.error(req.__('missing_post'), req.__('missing_post_code'))
      }

    });


  },

  answer: function (req, res) {

    if (!req.body.id || !req.body.message) {
      return res.error(req.__('missing_parameter_error'), req.__('missing_parameter_error_code'))
    }

    var postID = req.body.id;
    var message = req.body.message;
    Post.findOne(
      {
        id: postID,
        state: 'in progress',
        support: req.user.id
      }, function (err, post) {
        if (err) {
          return res.error(req.__('missing_post_assign'), req.__('missing_post_assign'));
        }
        else if (post) {
          // Consoante a fonte usar API diferente para responder

          PostLine.find(
            {
              where: {
                post: postID
              },
              sort: 'date ASC'
            },
            function (err, postline) {
              // Responder a este postline
              //console.log(postline);
              //console.log(postline[0]);
              var api = post.socialNetwork;
              var socialID = postline[0].socialID;
              var reply_to = postline[0].username;

              if (api === "facebook") {
                sails.services.facebook.replyPost(socialID, message,
                  function (err) {
                    if (err) {
                      sails.log.error(err);
                    }
                    else {
                      return res.success(req.__('reply_post_success'), req.__('reply_post_success_code'), message);
                    }
                  });
              }
              else if (api === "twitter") {
                sails.services.twitter.replyPost(socialID, reply_to, message,
                  function (data) {
                    if (data) {
                      PostLine.create(
                        {
                          socialID: data.tweetID,
                          message: data.text,
                          author: data.user.name,
                          username: data.user.username,
                          photoURL: data.user.photoURL,
                          date: new Date(data.date),
                          post: postID
                        },
                        function(err,pl){
                          if(err){
                            sails.log.err(err);
                          }
                        });
                      return res.success(req.__('reply_post_success'), req.__('reply_post_success_code'), message);
                    }
                  });
              }

            })
        }
        else {
          return res.error(req.__('missing_post_assign'), req.__('missing_post_assign_code'));
        }
      })


  },

  subscribePostSocket: function (req, res) {
    if (req.isSocket) {

      var id = req.param('id');
      var type = req.param('type');

      if (!type || !id)
        return res.error(req.__("missing_parameter_error"), req.__("missing_parameter_error_code"));

      var query;
      if (type === 'customer')
        query = Chat.find({customer: id});
      else if (type === 'support')
        query = Chat.find({support: id});
      else if (type === 'update') {
        query = Chat.find({});
      }
      else
        return res.error(req.__("missing_parameter_error"), req.__("missing_parameter_error_code"));

      query.exec(function (err, posts) {
        if (err || !posts) {
          sails.log.debug(err);
          return;
        }

        //sails.log.debug('User with ID ' + id + ' subscribed to ' + posts.length + ' posts');
        Post.subscribe(req, posts);
      });
    }
  },

  postUpdateSubscribe: function (req, res) {
    if (req.isSocket) {
      var query;
      query = Post.find({});
      query.exec(function (err, posts) {
        if (err || !posts) {
          sails.log.debug(err);
          return;
        }
        Post.subscribe(req, posts);
        Post.watch(req);
      });
    }
  },

  list: function (req, res) {
    if (!req.params.id) {
      return res.error(req.__('missing_parameter_error'), req.__('missing_parameter_error_code'))
    }

    var postID = req.params.id;
    var r = new Object();

    Post.findOne({id: postID}, function (err, post) {
      if (err) {
        return res.error(req.__('missing_post_assign'), req.__('missing_post_assign'));
      }
      else if (post) {
        r = post;
        PostLine.find({post: post.id, sort: 'date ASC'}, function (err, postlines) {
          if (err) {
            return res.error(req.__('missing_post_assign'), req.__('missing_post_assign'));
          }
          else if (postlines) {
            r.postlines = new Array();
            async.each(postlines,
              function (currentPL, cb) {
                PostReply.find({postline: currentPL.id, sort: 'date ASC'}, function (err, replies) {
                  if (err) {
                    return res.error(req.__('missing_post_assign'), req.__('missing_post_assign'));
                  }
                  else if (replies) {
                    currentPL.replies = replies;
                    r.postlines.push(currentPL);
                    cb();
                  }
                })

              },
              function (err) {
                if (err) {
                  return res.error(req.__('missing_post_assign'), req.__('missing_post_assign'));
                }
                else {

                  async.parallel({
                    enteredQueue: function(cb){
                      EnteredQueueDatetime.find({post: postID},
                        function(err,eqt){
                          cb(err,eqt);
                        }
                      );
                    },

                    exitedQueue: function(cb){
                      ExitedQueueDatetime.find({post:postID},
                        function(err,exqt){
                          cb(err,exqt);
                        }
                      );
                    },

                    solvedTime: function(cb){
                      SolvedDatetime.find({post: postID},
                        function(err,sdt){
                          cb(err,sdt);
                        }
                      );
                    }
                  },
                  function(err, results){
                    if(err) sails.log.error(err);
                    r.enteredQueueDatetime = results.enteredQueue;
                    r.exitedQueueDatetime = results.exitedQueue;
                    r.solvedTime = results.solvedTime;

                    return res.json(r);

                  });
                }
              })
          }
        })

      }
      else {
        return res.error(req.__('missing_post_assign'), req.__('missing_post_assign'));
      }

    });
  },

  refreshPost: function(req,res){
    if (!req.params.id) {
      return res.error(req.__('missing_parameter_error'), req.__('missing_parameter_error_code'))
    }

    var postID = req.params.id;

    Post.findOne(
      {
        id: postID,
      },
      function(err, dbpost){
        if (err || !dbpost) {
          return res.error(req.__('missing_post_assign'), req.__('missing_post_assign'));
        }
        else if (dbpost) {
          PostLine.findOne(
            {
              post: dbpost.id,
              sort: 'date ASC'
            },
            function(err,postline){
              if(err || !postline){
                return res.error(req.__('missing_post_assign'), req.__('missing_post_assign'));
              }
              else if(postline){
                var socialID = postline.socialID;
                if(dbpost.socialNetwork == "facebook"){
                  sails.services.facebook.fetchPost(socialID,
                    function(post){
                      if(post){
                        if(post.comments){
                          var returnData = dbpost;
                          returnData.postlines = new Array();
                          //Itera sobre os comentarios
                          async.each(post.comments.data,
                            function(comment, cb){
                              // A cada comentario do Post vai inserir se ainda nao existe na DB
                              var replyCommentLine = new Array();
                              PostLine.findOrCreate(
                                {
                                  socialID: comment.id
                                },
                                {
                                  socialID: comment.id,
                                  message: comment.message,
                                  author: comment.from.name,
                                  username: comment.from.name,
                                  photoURL: "http://graph.facebook.com/" + comment.from.id + "/picture?type=square&height=200&width=200",
                                  date: new Date(comment.created_time),
                                  post: dbpost.id
                                },
                                function(err, commentLine){
                                  if(err || !commentLine){
                                    sails.log.error("Erro a adicionar comentário de uma publicação à base de dados")
                                    sails.log.error(err);
                                  }
                                  else if(commentLine){
                                    // Analisar os replies a comentarios
                                    commentLine.replies = new Array();
                                    var replies = sails.services.facebook.fetchReplies(commentLine.socialID,
                                      function(replyData){
                                        async.each(replyData,
                                          function(currReply, replyCB){
                                            PostReply.findOrCreate(
                                              {
                                                socialID: currReply.id
                                              },
                                              {
                                                socialID: currReply.id,
                                                message: currReply.message,
                                                author: currReply.from.name,
                                                username: currReply.from.name,
                                                photoURL: "http://graph.facebook.com/" + currReply.from.id + "/picture?type=square&height=200&width=200",
                                                date: new Date(currReply.created_time),
                                                post: dbpost.id,
                                                postline: commentLine.id
                                              },
                                              function(err, replyLine){
                                                if(err || !replyLine){
                                                  sails.log.error("Erro a adicionar resposta a comentario de uma publicação à base de dados")
                                                  sails.log.error(err);
                                                }
                                                else{
                                                  commentLine.replies.push(replyLine);
                                                  replyCB();
                                                }
                                              }
                                            );
                                          },
                                          function(err){
                                            returnData.postlines.push(commentLine);
                                            cb();
                                          }
                                        );
                                      }
                                    );
                                  }
                                }
                              );
                            },
                            function(err){
                              return res.success(req.__('refresh_post_success'), req.__('refresh_post_success_code'),returnData);
                            }
                          );
                        }
                        else{
                          return res.success(req.__('refresh_post_success'), req.__('refresh_post_success_code'), {desc: "There were no new comments"});
                        }
                      }
                    }
                  );
                }
                else if(dbpost.socialNetwork == "twitter"){
                  // TODO: ir buscar Post e postlines - twitter na otem replies
                  var returnObj = new Object();
                  Post.findOne({id: postID}, function (err, post) {
                      if (err) {
                        return res.error(req.__('missing_post_assign'), req.__('missing_post_assign'));
                      }
                      else if (post) {
                        returnObj = post;
                        PostLine.find({post: post.id, sort: 'date ASC'}, function (err, postlines) {
                          if (err) {
                            return res.error(req.__('missing_post_assign'), req.__('missing_post_assign'));
                          }
                          else if (postlines) {
                            returnObj.postlines = postlines;
                            return res.success(req.__('refresh_post_success'), req.__('refresh_post_success_code'),returnObj);

                          }
                        });

                      }
                      else {
                        return res.error(req.__('missing_post_assign'), req.__('missing_post_assign'));
                      }

                    });
                }
              }
            }
          );
        }
        else{
          return res.error(req.__('missing_post_assign'), req.__('missing_post_assign_code'));
        }
      }
    );
  }
};

