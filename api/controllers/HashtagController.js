/**
 * HashtagController
 *
 * @description :: Server-side logic for managing Hashtags
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
  testSocket: function(req,res){
    if (req.isSocket){
      Hashtag.watch(req);
      console.log('User with socket id '+sails.sockets.id(req)+' is now subscribed to the model class \'hashtags\'.');
    }
  }
};

