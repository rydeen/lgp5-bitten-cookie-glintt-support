/**
 * SupportUserController
 *
 * @description :: Server-side logic for managing Supportusers
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var bcrypt = require('bcrypt-nodejs');
var async = require("async");

module.exports = {

    refresh: function(req,res){
        if(sails.services.socialnetworks.fetchPosts()){
            return res.success(req.__("refresh_sucess"), req.__("refresh_sucess_code"));
        }
        else{
            return res.error(req.__("refresh_error"), req.__("refresh_error_code"));
        }
    },

    subscribe: function (req, res) {

        if (req.isSocket) {
            var query;
            query = SupportUser.find({});
            query.exec(function (err, users) {
                if (err || !users) {
                    sails.log.debug(err);
                    return;
                }

                SupportUser.subscribe(req, users);
                SupportUser.watch(req);
            });
        }
    },

    changePassword: function(req,res){
        if (!req.body || !req.body.new || !req.body.old) {
            return res.error(req.__('missing_parameter_error'), req.__('missing_parameter_error_code'))
        }
        if (!req.user){
            return res.error(req.__("user_not_logged_error"), req.__("user_not_logged_error_code"));
        }

        var oldPw = req.body.old;
        var newPw = req.body.new;

        var userID = req.user.id;

        SupportUser.findOne({id: userID},
            function(err,support){
                if(err){
                    sails.log.error(err);
                }
                else if(support){
                    // Comparar pw velha recebida com hash da db
                    bcrypt.compare(oldPw, support.password, function(err, response) {
                        // Se der erro a comparar
                        if(err){
                            sails.log.error(err);
                        }
                        else if(response){ // A pass dada e verdadeira

                            var newHash = bcrypt.hashSync(newPw);

                            SupportUser.update({id: userID},
                                {
                                    password: newHash
                                },
                                function(err,supp){
                                    if(err){
                                        sails.log.error(err);
                                    }
                                    else{
                                        return res.success(req.__("password_change_success"), req.__("password_change_success_code"), support.username);
                                    }
                                }
                            );
                        }
                        else{
                            return res.error(req.__("old_password_incorrect_error"), req.__("old_password_incorrect_error_code"));
                        }
                    });

                }
            }
        );
    },

    listAll: function (req, res) {
        var r = [];

        SupportUser.find({}, function (err, users){
                if (err) {
                    return res.error(req.__('missing_post_assign'), req.__('missing_post_assign'));
                }
                else if (users){
                    r = users;

                    async.forEachOf(users,
                        function(currentUser, userKey, cbForEach){

                            //posts
                            async.parallel({
                                    fetchPosts: function(callback){
                                        Post.find({support: currentUser.id}, function(err,posts){
                                            if (err) {
                                                return res.error(req.__('missing_post_assign'), req.__('missing_post_assign'));
                                            }
                                            else if (posts) {
                                                var p = [];
                                                async.each(posts,
                                                    function(currentPost, cb2){

                                                        SolvedDatetime.find({ where: {post: currentPost.id}, limit: 1, sort: 'id DESC'}, function(err, times){
                                                            if (err) {
                                                                return res.error(req.__('missing_post_assign'), req.__('missing_post_assign'));
                                                            }
                                                            else if (times) {
                                                                if( times.length > 0)
                                                                    currentPost.solvedTime = times[0].date;
                                                                else currentPost.solvedTime = null;

                                                                p.push(currentPost);
                                                                cb2();
                                                            }

                                                        })
                                                    },
                                                    function(err){
                                                        callback(null,p); //acabou de tratar dos posts
                                                    })
                                            }
                                        })
                                    },
                                    fetchChats: function(callback){
                                        Chat.find({support: currentUser.id}, function(err,chats){
                                            if (err) {
                                                return res.error(req.__('missing_post_assign'), req.__('missing_post_assign'));
                                            }
                                            else if (chats) {
                                                var c = [];
                                                async.each(chats,
                                                    function(currentChat, cb2){

                                                        SolvedDatetime.find({ where: {chat: currentChat.id}, limit: 1, sort: 'id DESC'}, function(err, times){
                                                            if (err) {
                                                                return res.error(req.__('missing_post_assign'), req.__('missing_post_assign'));
                                                            }
                                                            else if (times) {
                                                                if( times.length > 0)
                                                                    currentChat.solvedTime = times[0].date;
                                                                else currentChat.solvedTime = null;

                                                                c.push(currentChat);
                                                                cb2();
                                                            }

                                                        })
                                                    },
                                                    function(err){
                                                        callback(null, c); //acabou de tratar dos posts
                                                    })
                                            }
                                        })
                                    }

                                },
                                function(err, results){
                                    r[userKey].userposts = results.fetchPosts;
                                    r[userKey].userchats = results.fetchChats;

                                    cbForEach();
                                }
                            );
                        }, function(err){
                            return res.json(r);
                        })
                }
            }
        );
    }

};

