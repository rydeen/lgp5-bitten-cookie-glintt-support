/**
 * ChatController
 *
 * @description :: Server-side logic for managing Chats
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

  getThisYear: function (req, res) {
    Chat.find({createdAt: {'>': new Date('1/1/' + new Date().getFullYear())}})
      //.populate('postLines')
      .populate('exitedQueue')
      .populate('solvedDatetime')
      .populate('enteredQueue')
      .exec(function (err, posts) {
        if (err) {
          sails.log.error(err);

          return res.error(req.__("this_year_chats_error"), req.__("this_year_chats_error_code"));
        }

        res.send(posts);
      });
  },

  chatUpdateSubscribe: function (req, res) {

    if (req.isSocket) {
      var query;
      query = Chat.find({});
      query.exec(function (err, chats) {
        if (err || !chats) {
          sails.log.error(err);
          return;
        }
        Chat.subscribe(req, chats);
        Chat.watch(req);
      });
    }
  },

  addChat: function (req, res) {

    if (!req.body || !req.body.customer || !req.body.support || !req.body.date) {
      return res.error(req.__("missing_parameter_error"), req.__("missing_parameter_error_code"));
    }

    Chat.update({customer: req.body.customer}, {
      support: req.body.support,
      state: 'in progress'
    })
      .exec(function (err, chat) {
        if (err || !chat) {
          sails.log.error(err);
          return;
        }
        Chat.publishUpdate(chat[0].id);
        Chat.updateExitedQueue(chat[0].id, req.user.id);
      });
  },

  resolveChat: function (req, res) {

    if (!req.body || !req.body.customer || !req.body.support ) {
      return res.error(req.__("missing_parameter_error"), req.__("missing_parameter_error_code"));
    }

    Chat.update({customer: req.body.customer}, {
      support: req.body.support,
      state: 'solved'
    })
      .exec(function (err, chat) {
        if (err || !chat) {
          sails.log.error(err);
          return;
        }
        Chat.publishUpdate(chat[0].id);
        Chat.updateSolvedTime(chat[0].id, req.body.support);
      });
  },
  subscribeChatSocket: function (req, res) {
    if (req.isSocket) {
      var id = req.param('id');
      var type = req.param('type');

      if (!type || !id)
        return res.error(req.__("missing_parameter_error"), req.__("missing_parameter_error_code"));

      var query;
      if (type === 'customer')
        query = Chat.find({customer: id});
      else if (type === 'support')
        query = Chat.find({support: id});
      else if (type === 'update') {
        query = Chat.find({});
      }
      else
        return res.error(req.__("missing_parameter_error"), req.__("missing_parameter_error_code"));

      query.exec(function (err, chats) {
        if (err || !chats) {
          sails.log.error(err);
          return;
        }

        //sails.log.debug('User with ID ' + id + ' subscribed to ' + chats.length + ' chats');
        Chat.subscribe(req, chats);
      });
    }
  }
};

