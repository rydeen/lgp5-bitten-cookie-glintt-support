/**SocialController
 *
 * @description :: Apenas para demonstrar como usar os serviços Facebook e Twitter
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	facebookPagefeed: function (req, res) {
	  	var FB = sails.services.facebook.getAppAccess();
	  	var FacebookPageID = sails.services.facebook.getFacebookPageID();

	    FB.api(FacebookPageID + '/feed/', 
	    	{
	    		include_hidden : false
	    	}, 
	    	function (result) {
	        return res.json(result);
	    });
	
  	},

  	twitterSearch: function (req, res) {
	  	var T = sails.services.twitter.getAppAccess();

	    var requestedword = req.params.hashtag;
		T.get('search/tweets', { q: '#' + requestedword, count: 10 }, function (err, data, response) {
			if(err) return res.json(err);
			else return res.json(data);
		})
	
  	},

  	session: function(req,res){

  		return res.json(req.session);
  	}

	
};
