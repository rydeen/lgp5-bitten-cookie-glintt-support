/**
 * AuthController
 *
 * @description :: Server-side logic for managing Supportusers
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
var passport = require('passport');
module.exports = {
  login: function (req, res) {

    if (!req.body || !req.body.username || !req.body.password) {
      return res.error(req.__("missing_parameter_error"), req.__("missing_parameter_error_code"));
    }

    passport.authenticate('local', function (err, user, info) {
      if ((err) || (!user)) {
        return res.error(req.__("login_wrong_password_error"), req.__("login_wrong_password_error_code"));
      }
      req.logIn(user, function (err) {
        if (err) {
          return res.error(req.__("login_error"), req.__("login_error_code"));
        }

        if (user.permissions == "support") {
          req.session.permissions = "support";
          req.session.authenticated = true;
          var now = new Date();
          SupportUser.update(
            {
              id: user.id
            },
            {
              LastLoginDate: now
            },
            function(err,us){
              if(err){
                sails.log.err(err);
              }
              else{
                user.LastLoginDate = now;
                SupportUser.publishUpdate(user.id);
                return res.success(req.__("user_login_success"), req.__("user_login_success_code"), user);
              }
            }
          );

        }
        else {
          req.session.permissions = "supervisor";
          return res.success(req.__("user_login_success"), req.__("user_login_success_code"), user);
        }

        

        
      });
    })(req, res);
  },

  logout: function (req, res) {
    req.logout();
    req.session.destroy();
    //res.redirect('/loginpage');
    return res.success(req.__("user_logout_success"), req.__("user_logout_success_code"));
  },

  info: function (req, res) {
    if(req.user){
      if(req.user.password){
        delete req.user.password;
        return res.success(req.__("user_data_sucess"), req.__("user_data_sucess_code"), req.user);
      }
      else {
        return res.success(req.__("user_data_sucess"), req.__("user_data_sucess_code"), req.user);
      }
    }
    else return res.error(req.__("user_not_logged_error"), req.__("user_not_logged_error_code"));
  }

};
