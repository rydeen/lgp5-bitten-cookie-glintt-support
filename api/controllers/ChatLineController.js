/**
 * ChatLineController
 *
 * @description :: Server-side logic for managing Chatlines
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
  newChatLine: function (req, res) {
    if (!req.body || !req.body.message || !req.body.message || !req.body.from) {
      return res.error(req.__("missing_parameter_error"), req.__("missing_parameter_error_code"));
    }

    ChatLine.create({chat: req.body.chat, from: req.body.from, message: req.body.message})
      .exec(function (err, line) {
        if (err || !line) {
          sails.log.error(err);

          return res.error(req.__("chatline_added_error"), req.__("chatline_added_error_code"));
        }

        return res.success(req.__("chatline_added_success"), req.__("chatline_added_success_code"));
      });
  }
};

