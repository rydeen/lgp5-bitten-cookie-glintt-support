/**
 * SupervisorUser.js
 *
 * @description :: Person with a bird's eye view over all SupportUsers.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

var bcrypt = require('bcrypt-nodejs')

module.exports = {

  attributes: {
    username: {
      type: 'string',
      required: true
    },

    password: {
      type: 'string',
      required: true
    }
  },

  seedData: [
    {
      id: 101,
      username: 'supervisor',
      password: ','
    }
  ],

  beforeCreate: function(user, cb) {
    bcrypt.genSalt(10, function(err, salt) {
      bcrypt.hash(user.password, salt, null, function(err, hash) {
        if (err) {
          //console.log(err);
          cb(err);
        }else{
          user.password = hash;
          cb(null, user);
        }
      });
    });
  }
};

