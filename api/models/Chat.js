/**
 * Chat.js
 *
 * @description :: A new chat opened by the end-user (customer) to receive support.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

  attributes: {
    exitedQueue: {
      collection: 'ExitedQueueDatetime',
      via: 'post'
    },

    solvedDatetime: {
      collection: 'SolvedDatetime',
      via: 'post'
    },

    enteredQueue: {
      collection: 'EnteredQueueDatetime',
      via: 'post'
    },

    state: {
      type: 'string',
      enum: ['queue', 'in progress', 'solved'],
      required: true
    },

    customer: {
      model: 'CustomerUser'
    },

    support: {
      model: 'SupportUser'
    },

    chatLines: {
      collection: 'ChatLine',
      via: 'chat'
    }
  },

  updateExitedQueue : function(chatID, supportID){
     ExitedQueueDatetime.create(
        {
           support: supportID,
           date: new Date(),
           chat: chatID
        },
        function(err,dt){
           if(err) sails.log.error(err);
        }
     );
  },

  updateSolvedTime : function(chatID, supportID){
     SolvedDatetime.create(
        {
           support: supportID,
           date: new Date(),
           chat: chatID
        },
        function(err,dt){
           if(err) sails.log.error(err);
        }
     );
  },

  queueChat: function(chatID){
     EnteredQueueDatetime.create(
      {
        date: new Date(),
        chat: chatID
      },
      function(err,dt){
        if(err){ sails.log.error(err); cb(err, null); }
      }
    );
  }

};

