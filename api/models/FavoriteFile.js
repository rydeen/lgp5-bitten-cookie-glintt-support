/**
* FavoriteFile.js
*
* @description :: Common file that all SupportUsers can re-use.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    dropboxUrl: {
      type: 'text',
      required: true,
      unique: true
    }
  },

  seedData: [
    {
      dropboxUrl: 'https://www.dropbox.com/s/87obwltbjgo1mte/Introdu%C3%A7%C3%A3o.pdf?dl=0'
    }
  ]
};

