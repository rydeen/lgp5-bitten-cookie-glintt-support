/**
 * ChatLine.js
 *
 * @description :: A message within a chat.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

  attributes: {
    message: {
      type: 'text',
      required: true
    },

    //Support name
    from: {
      type: 'string',
      //enum: ['customer', 'support'],
      required: true
    },

    chat: {
      model: 'chat'
    }
  },

  seedData: [],

  afterCreate: function (chatline, cb) {
      Chat.publishAdd(chatline.chat, 'chatLines', chatline.id, chatline.chat);

      if (chatline.from == 'customer') {
      Chat.update({id: chatline.chat, state: 'solved'}, {state: 'queue'}).exec(function (err, chat) {
              //sails.log.debug("Updated!");

              if (err || !chat)
                  sails.log.error(err);

              else Chat.queueChat(chatline.chat);
          });
      }
    cb();
  }
};

