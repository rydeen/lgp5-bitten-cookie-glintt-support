/**
* CustomerUser.js
*
* @description :: The end-user, seeking support.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    username: {
      type: 'string',
      required: true
    },

    birthday: {
      type: 'date'
    },

    email: {
      type: 'email'
    },

    phoneNumber: {
      type: 'string',
      size: 15
    },

    address: {
      type: 'text'
    },

    socialNetworkID: {
      type: 'string',
      required: true,
      unique: true
    },

    photoURL: {
      type: 'string'
    },

    socialNetwork: {
      type: 'string',
      enum: ['google', 'twitter', 'facebook'],
      required: true
    },

    chat: {
      model: 'Chat'
    }
  },

  // Create a chat for every customer
  afterCreate: function(user, cb) {
    Chat.create({state: 'queue', customer: user.id})
      .exec(function(err, chat) {
      if(err || !chat) {
        sails.log.error(err);
        return;
      }
      Chat.publishCreate({id:user.id,chat:chat.id});

      CustomerUser.update(user.id, {chat: chat.id}).exec(function(err) {
        if(err) {
          sails.log.error(err);
          return;
        }

        cb();
      })
    });


  },

  seedData: [
    {
      id: 1,
      username: 'Manel',
      socialNetworkID: '1332345',
      socialNetwork: 'google'
    },
    {
      id: 2,
      username: 'Maria',
      socialNetworkID: '985769845',
      socialNetwork: 'facebook'
    },
    {
      id: 3,
      username: 'Gabriel Candal',
      socialNetworkID: '2504232605',
      socialNetwork: 'twitter'
    }
  ]
};


