/**
 * Hashtag.js
 *
 * @description :: Hashtag to be monitored.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

  attributes: {
    hashtag: {
      type: 'text',
      required: true,
      unique: true
    }
  },

  seedData: [
    {
      hashtag: 'cuf'
    },
    {
      hashtag: 'glintt'
    }
  ]
};

