/**
 * SupportUser.js
 *
 * @description :: Person giving support to the end-user (customer).
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

var bcrypt = require('bcrypt-nodejs');

module.exports = {

  attributes: {
    username: {
      type: 'string',
      required: true,
      unique: true
    },

    password: {
      type: 'string',
      required: true
    },

    chats: {
      collection: 'Chat',
      via: 'support'
    },

    posts: {
      collection: 'Post',
      via: 'support'
    },

    LastLoginDate: {
      type: 'datetime'
    }
  },

  seedData: [
    {
      id: 1,
      username: 'support1',
      password: ','
    },
    {
      id: 2,
      username: 'support2',
      password: ','
    }
  ],

  beforeCreate: function(user, cb) {
    bcrypt.genSalt(10, function(err, salt) {
      bcrypt.hash(user.password, salt, null, function(err, hash) {
        if (err) {
          //console.log(err);
          cb(err);
        }else{
          user.password = hash;
          cb(null, user);
        }
      });
    });
  }
};

