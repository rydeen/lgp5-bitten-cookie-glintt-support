/**
* PostReply.js
*
* @description :: One of the replies of the messages within (facebook)
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    message: {
      type: 'text',
      required: true
    },

    author: {
      type: 'string',
      required: true
    },

    username: {
      type: 'string'
    },

    socialID: {
      type: 'string',
      required: true,
    },

    photoURL: {
      type: 'string'
    },

    post: {
      model: 'post'
    },

    postline: {
      model: 'postline'
    },

    date: {
      type: 'datetime'
    },

  },

};


