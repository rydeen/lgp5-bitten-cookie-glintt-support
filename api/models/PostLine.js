/**
* PostLine.js
*
* @description :: One of the messages within
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    message: {
      type: 'text',
      required: true
    },

    author: {
      type: 'string',
      required: true
    },

    username: {
      type: 'string'
    },

    socialID: {
      type: 'string',
      required: true,
    },

    photoURL: {
      type: 'string'
    },

    post: {
      model: 'post'
    },

    date: {
      type: 'datetime'
    },

    replies: {
      collection: 'PostReply',
      via: 'postline'
    }
  },

  countReplies: function(postlineID, cb){
    var counter = 0;
    PostReply.find({postline: postlineID}, function(err,lines){
      if(err){ sails.log.error(err); cb(err,null); }
      else if(lines){
        counter += lines.length;
        cb(null,counter);
      }
    })
  },

  seedData: [
    {
      id: 1,
      post: 1,
      author: 'Manel',
      message: 'Olá fofos',
      socialID: '1',
      photoURL: 'http://www.oficinadolivro.pt/fotos/produtos/500_9789895553273_doces_manel.jpg'
    },
    {
      id: 2,
      post: 1,
      author: 'Quim',
      message: 'Olá Manel',
      socialID: '2'
    },
    {
      id: 3,
      post: 1,
      author: 'Joaquina',
      message: 'E quê?',
      socialID: '3',
      photoURL: 'http://asktabby.files.wordpress.com/2013/03/wpid-photo-mar-6-2013-1015-pm.jpg'
    },
    {
      id: 4,
      post: 2,
      author: 'Manel',
      message: 'Olá novamente',
      socialID: '4',
      photoURL: 'http://www.oficinadolivro.pt/fotos/produtos/500_9789895553273_doces_manel.jpg'
    },
    {
      id: 5,
      post: 3,
      author: 'Joaquina',
      message: 'Acho o vosso logotipo ofensivo!',
      socialID: '5',
      photoURL: 'http://asktabby.files.wordpress.com/2013/03/wpid-photo-mar-6-2013-1015-pm.jpg'
    }
  ]
};


