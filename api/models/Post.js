/**
 * Post.js
 *
 * @description :: The root post (meaning, the first one in a series) in a social-network which was marked as interesting
 * (by hashtag or presence in a social media page).
 *
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

  attributes: {
    exitedQueue: {
      collection: 'ExitedQueueDatetime',
      via: 'post'
    },

    solvedDatetime: {
      collection: 'SolvedDatetime',
      via: 'post'
    },

    enteredQueue: {
      collection: 'EnteredQueueDatetime',
      via: 'post'
    },

    date: {
      type: 'datetime'
    },

    state: {
      type: 'string',
      enum: ['queue', 'in progress', 'solved'],
      required: true
    },

    support: {
      model: 'SupportUser'
    },

    postLines: {
      collection: 'PostLine',
      via: 'post'
    },

    socialNetwork: {
      type: 'string'
    }
  },

  updateExitedQueue : function(postID, supportID){
    ExitedQueueDatetime.create(
      {
        support: supportID,
        date: new Date(),
        post: postID
      },
      function(err,dt){
        if(err) sails.log.error(err);
      }
    );
  },

  updateSolvedTime : function(postID, supportID){
    SolvedDatetime.create(
      {
        support: supportID,
        date: new Date(),
        post: postID
      },
      function(err,dt){
        if(err) sails.log.error(err);
      }
    );
  },

  queuePost: function(postID){
    EnteredQueueDatetime.create(
      {
        date: new Date(),
        post: postID
      },
      function(err,dt){
        if(err){ sails.log.error(err); cb(err, null); } 
      }
    );
  },

  reQueuePost : function(postID){
    Post.find(postID, function(err,postRec){
      if(postRec[0]){
        var post = postRec[0];
        if(post.state === 'solved'){
          EnteredQueueDatetime.create(
            {
              date: new Date(),
              post: postID
            },
            function(err,dt){
              if(err){ sails.log.error(err); cb(err, null); } 
            }
          );
          Post.update(
            {
              id: postID
            },
            {
              state: "queue"
            },
            function(err,post){
              if(err){ sails.log.error(err); cb(err, null); }
              sails.log.debug("REQUEUE com sucesso");
            }
          );
        }
        else{
          sails.log.debug("Detetada diferenca em relacao a DB. Nao e preciso REQUEUE, ja esta para alguem ou em queue")
        }
      }
    });
  },

  countLines: function(postID, cb){
    var counter = 0;
    PostLine.find({post: postID}, function(err,lines){
      if(err){ sails.log.error(err); cb(err,null); }
      else if(lines){
        counter += lines.length;
        cb(null,counter);
      }
    })
  },

  seedData: [
    {
      id: 1,
      state: 'queue'
    },
    {
      id: 2,
      support: 1,
      state: 'solved',
      exitedQueueDatetime: new Date('01-04-15 13:41:12')
    },
    {
      id: 3,
      support: 2,
      state: 'in progress',
      exitedQueueDatetime: new Date('05-04-15 15:41:12'),
      solvedDatetime: (function () {
        var date = new Date('05-04-15 15:41:12');

        date.setDate(date.getDate() + 5);

        return date;
      })()
    }
  ]
};