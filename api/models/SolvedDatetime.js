/**
* SolvedDatetime.js
*
* @description :: Time that a Post left Queue
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    date: {
      type: 'datetime'
    },

    post: {
      model: 'post'
    },
    
    chat: {
      model: 'chat'
    },

    support: {
      model: 'supportuser'
    },

  },

};

